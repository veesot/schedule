import random
from faker import Factory

from timetable.models import Tutor, Discipline

fake = Factory.create('ru_RU')


def create_tutors():
    for _ in range(0, 2):
        tutor = Tutor()
        fake_f_name, fake_l_name = fake.name().split(' ')
        tutor.f_name = fake_f_name
        tutor.l_name = fake_l_name
        tutor.save()


def add_tutors_to_discipline():
    disciplines_without_tutor = Discipline.objects.all()
    all_tutors = list(Tutor.objects.all())
    for discipline in disciplines_without_tutor:
        tutors = discipline.tutors.all()
        if not tutors:
            discipline.tutors.add(random.choice(all_tutors))
