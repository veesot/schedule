import json
from typing import List

import requests
from django.db import models
from django.http import HttpResponse

from auditories.models import Auditory
from main.settings import ACCOUNTS_API
from service.service import IntegerListField, query_set_to_dict


class Discipline(models.Model):
    title = models.CharField(max_length=30, verbose_name='Наименование дисциплины')
    tutors = IntegerListField()
    auditories = models.ManyToManyField(Auditory, verbose_name="Список аудиторий подходящих дисциплине", blank=True)

    def get_tutor_id_list(self) -> List[int]:
        if self.tutors:
            return json.loads(self.tutors)
        else:
            return []

    def remove_tutor(self, tutor_id: int) -> bool:
        tutor_id_list = self.get_tutor_id_list()
        tutor_id_list.remove(tutor_id)
        return self.update_tutors(tutor_id_list)

    def update_tutors(self, tutor_id_list: List[int]) -> bool:
        self.tutors = str(tutor_id_list)
        if self.save():
            return True
        else:
            return False

    def add_tutor(self, tutor_id: int) -> bool:
        tutor_id_list = self.get_tutor_id_list()
        tutor_id_list.append(tutor_id)
        return self.update_tutors(tutor_id_list)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'disciplines'
        app_label = 'disciplines'
        verbose_name = 'Дисциплины'
        verbose_name_plural = "Дисциплина"


class Api:
    @classmethod
    def index(cls, request):
        tutors = request.GET.get('tutors', None)
        auditories = request.GET.get('auditories', None)
        from timetable.models.timetable import Timetable
        current_timetable = Timetable.objects.get(title=request.GET.get('timetable'))
        disciplines_all = current_timetable.disciplines.all()
        disciplines = query_set_to_dict(disciplines_all, 'title', 'id')
        for discipline in disciplines:
            current_discipline = Discipline.objects.get(id=discipline['id'])
            if tutors == 'true' and current_discipline.tutors:
                tutor_list_id = json.loads(current_discipline.tutors)
                discipline['tutors'] = []
                for tutor_id in tutor_list_id:
                    r = requests.get(ACCOUNTS_API + str(tutor_id))
                    tutor = r.json()
                    discipline['tutors'].append(tutor)
            if auditories == 'true':
                discipline['auditories'] = query_set_to_dict(current_discipline.auditories.all())

        return HttpResponse(json.dumps(disciplines), content_type="application/json")

    @classmethod
    def dispatcher_discipline(cls, request, discipline_id=None):

        if request.method == 'GET':  # Пришел запрос на индекс
            return cls.index(request)
        elif request.method == 'POST':  # Запрос на создание
            return cls.add(request)
        elif request.method == 'DELETE' and discipline_id:
            return cls.remove(discipline_id)
        elif request.method == 'PATCH' and discipline_id:
            return cls.update(request, discipline_id)

    @staticmethod
    def get(discipline):
        """
        Возвращаем метаинфо по дисциплине
        """
        discipline = Discipline.objects.get(title=discipline)
        response = {'title': discipline.title, 'id': discipline.id}

        return HttpResponse(json.dumps(response), content_type="application/json")

    @staticmethod
    def update(request, discipline_id):
        """
        Обновляем метаинфо по дисциплине

        """

        metadata = json.loads(request.body.decode())

        discipline = Discipline.objects.get(id=discipline_id)
        tutor_id_list = discipline.get_tutor_id_list()

        if 'tutor' in metadata:
            tutor_id = metadata.get('tutor').get('id')
            if tutor_id in tutor_id_list:  # Если изменяемый преподователь - есть - его удаляем.Иначе добавляем
                discipline.remove_tutor(tutor_id)
                response = {'status': 'Tutor removed'}
            else:
                discipline.add_tutor(tutor_id)
                response = {'status': 'Tutor added'}
            return HttpResponse(json.dumps(response), content_type="application/json")
        elif 'auditory' in metadata:  # Если пришел запрос на изменение аудитории
            auditory_meta_info = metadata['auditory']

            discipline = Discipline.objects.get(id=discipline_id)
            auditory = Auditory.objects.get(**auditory_meta_info)
            discipline_auditories = discipline.auditories.all()

            if auditory in discipline_auditories:
                discipline.auditories.remove(auditory)
            else:
                discipline.auditories.add(auditory)

            all_auditories = discipline.auditories.all()
            auditories = query_set_to_dict(all_auditories, 'number', 'id')

            return HttpResponse(json.dumps(auditories), content_type="application/json")

    @staticmethod
    def add(request):
        """
        Добавляем дисциплину
        """
        metadata = json.loads(request.body.decode())
        title = metadata['title']
        timetable = metadata['timetable']
        if not Discipline.objects.filter(title=title).exists():  # На случай если захотят добавят дубль

            new_discipline = Discipline()
            new_discipline.title = title
            new_discipline.save()

            from timetable.models.timetable import Timetable
            timetable = Timetable.objects.get(title=timetable)
            timetable.disciplines.add(new_discipline)

            response = {'title': title, 'id': new_discipline.id}
        else:
            response = {'title': title, 'id': None}

        return HttpResponse(json.dumps(response), content_type="application/json")

    @staticmethod
    def remove(discipline_id):
        """
        Удаляем дисциплину
        """
        discipline = Discipline.objects.get(id=discipline_id)
        timetable = discipline.timetable_set.get()
        timetable.disciplines.remove(discipline)
        discipline.delete()

        response = {'title': discipline.title, 'id': None}

        return HttpResponse(json.dumps(response), content_type="application/json")
