from django.conf.urls import url

from .models import Api

urlpatterns = [url(r'^$', Api.dispatcher_discipline),
               url(r'^(?P<discipline_id>\d+)/$', Api.dispatcher_discipline), ]
