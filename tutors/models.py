import json

import requests
from django.http import HttpResponse

from main.settings import GROUPS_API, ACCOUNTS_API, ACCOUNTS_TOKEN, TUTORS_API
from service.service import IntegerListField


class Tutor(IntegerListField):
    @classmethod
    def all(cls):
        groups = requests.get(GROUPS_API)
        tutors_group = next(filter(lambda group: group.get('name') == "tutors", groups.json()))
        tutors_group_id = tutors_group.get('id')
        params = {'group': tutors_group_id}
        r = requests.get(ACCOUNTS_API, params=params)
        response = r.json()
        return response

    @classmethod
    def get(cls, idx):
        r = requests.get(ACCOUNTS_API + idx)
        response = r.json()
        return response


class Api:
    @classmethod
    def dispatcher_tutors(cls, request):
        if request.method == 'GET':
            return cls.index()
        elif request.method == 'POST':
            return cls.add(json.loads(request.body.decode()))

    @staticmethod
    def add(metadata):
        user_info = {
            "username": '{first_name} {last_name}'.format(first_name=metadata['first_name'],
                                                          last_name=metadata['last_name']),
            "first_name": metadata['first_name'],
            "last_name": metadata["last_name"],
            "middle_name": metadata['middle_name']
        }
        headers = {'content-type': 'application/json', 'Authorization': ACCOUNTS_TOKEN}
        r = requests.post(TUTORS_API, data=json.dumps(user_info), headers=headers)
        response = r.json()
        return HttpResponse(json.dumps(response), content_type="application/json")

    @classmethod
    def index(cls):
        groups = requests.get(GROUPS_API)
        tutors_group = next(filter(lambda group: group.get('name') == "tutors", groups.json()))
        tutors_group_id = tutors_group.get('id')
        r = requests.get(GROUPS_API + '{idx}/accounts/'.format(idx=tutors_group_id))

        response = r.json()
        # first_names = [tutor['first_name'] for tutor in response]
        # last_names = [tutor['last_name'] for tutor in response]
        # middle_names = [tutor['middle_name'] for tutor in response]
        # response.append({'first_names': first_names, 'last_names': last_names, 'middle_names': middle_names})

        return HttpResponse(json.dumps(response), content_type="application/json")
