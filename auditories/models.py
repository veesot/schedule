import json

from django.db import models
from django.http import HttpResponse

from service.service import query_set_to_dict


class Auditory(models.Model):
    number = models.PositiveIntegerField(verbose_name='Номер аудитории')

    def __str__(self):
        return str(self.number)

    class Meta:
        app_label = 'auditories'
        db_table = 'auditories'


class Api:
    @classmethod
    def dispatcher_auditory(cls, request):
        if request.method == 'GET':
            return cls.get()
        elif request.method == 'POST':
            metadata = json.loads(request.body.decode())
            return cls.add(metadata)

    @staticmethod
    def add(metadata):
        auditory = Auditory.objects.filter(number=metadata['number'])
        if not auditory.exists():
            auditory = Auditory(**metadata)
            auditory.save()
        else:
            auditory = Auditory.objects.get(number=metadata['number'])

        metadata['id'] = auditory.id
        return HttpResponse(json.dumps(metadata), content_type="application/json")

    @classmethod
    def get(cls):
        all_auditories = Auditory.objects.all()
        auditories = query_set_to_dict(all_auditories, 'number', 'id')
        return HttpResponse(json.dumps(auditories), content_type="application/json")
