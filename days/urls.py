from django.conf.urls import url

from .models import BannedDayApi, StudyDayApi, IgnoredDayApi

urlpatterns = [url(r'^banned/$', BannedDayApi.dispatcher),
               url(r'^banned/(?P<banned_day_id>\d+)/$', BannedDayApi.dispatcher),
               url(r'^study/$', StudyDayApi.dispatcher),
               url(r'^study/(?P<study_day_id>\d+)$', StudyDayApi.dispatcher),
               url(r'^ignored/$', IgnoredDayApi.dispatcher),
               url(r'^ignored/(?P<ignored_day_id>\d+)$', IgnoredDayApi.dispatcher), ]
