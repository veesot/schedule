import datetime
import json

import locale
from django.db import models
from django.http import HttpResponse

from disciplines.models import Discipline
from pairs.models import Pair
from service.service import query_set_to_dict


class IgnoredDay(models.Model):
    beginning_of_period = models.DateField(verbose_name='Дата начала действия')
    end_of_period = models.DateField(verbose_name='Дата окончания действия')

    class Meta:
        app_label = 'days'
        db_table = 'ignored_days'


class StudyDay(models.Model):
    title = models.CharField(max_length=20, verbose_name='Наименование дня')
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        app_label = 'days'
        db_table = 'study_days'


class BannedDay(models.Model):
    discipline = models.ForeignKey(Discipline, verbose_name="Название дисциплины")
    pair = models.ForeignKey(Pair, verbose_name="Номер пары")
    day = models.ForeignKey(StudyDay, verbose_name="Учебный день")

    class Meta:
        db_table = 'banned_periods_discipline'
        app_label = 'days'


# API
class BannedDayApi:
    @classmethod
    def dispatcher(cls, request, banned_day_id=None):
        if request.method == 'GET':
            title_timetable = request.GET.get('timetable')
            return cls.get_banned_disciplines(title_timetable)
        elif request.method == 'POST':
            metadata = json.loads(request.body.decode())
            return cls.add(metadata)
        elif request.method == 'DELETE' and banned_day_id:
            return cls.remove(banned_day_id)

    @classmethod
    def get_banned_disciplines(cls, title_timetable):
        from timetable.models.timetable import Timetable
        current_timetable = Timetable.objects.get(title=title_timetable)
        all_days = current_timetable.get_index_banned_disciplines()
        return HttpResponse(json.dumps(all_days), content_type="application/json")

    @classmethod
    def add(cls, metadata):
        title_timetable = metadata.get('timetable')
        banned_day = BannedDay()
        banned_day.day = StudyDay.objects.get(title=metadata['day'])
        banned_day.discipline = Discipline.objects.get(title=metadata['discipline'])
        banned_day.pair = Pair.objects.get(title=metadata['pair'])
        banned_day.save()
        from timetable.models.timetable import Timetable
        timetable = Timetable.objects.get(title=title_timetable)
        timetable.banned_days.add(banned_day)

        response = {'id': banned_day.id,
                    'pair': banned_day.pair.title,
                    'discipline': banned_day.discipline.title,
                    'day': banned_day.day.title}
        return HttpResponse(json.dumps(response), content_type="application/json")

    @classmethod
    def remove(cls, banned_day_id):
        banned_day = BannedDay.objects.get(id=banned_day_id)
        timetable = banned_day.timetable_set.get()
        timetable.banned_days.remove(banned_day)
        banned_day.delete()
        response = []
        return HttpResponse(json.dumps(response), content_type="application/json")


class StudyDayApi:
    @classmethod
    def dispatcher(cls, request, study_day_id=None):
        if request.method == 'GET':
            return cls.get_days()
        elif request.method == 'PATCH' and study_day_id:
            return cls.update(study_day_id)

    @classmethod
    def get_days(cls):
        all_days = StudyDay.objects.all()
        response = query_set_to_dict(all_days)
        for day in response:
            day['is_active'] = StudyDay.objects.get(id=day['id']).is_active
        return HttpResponse(json.dumps(response), content_type="application/json")

    @classmethod
    def update(cls, study_day_id):
        day = StudyDay.objects.get(id=study_day_id)
        day.is_active = not day.is_active  # Inverse current value
        day.save()
        response = {'is_active': day.is_active, 'title': day.title, 'id': day.id}
        return HttpResponse(json.dumps(response), content_type="application/json")


class IgnoredDayApi:
    @classmethod
    def dispatcher(cls, request, ignored_day_id=None):
        if request.method == 'GET' and ignored_day_id is None:
            title_timetable = request.GET.get('timetable')
            return cls.index(title_timetable)
        elif request.method == 'POST':
            metadata = json.loads(request.body.decode())
            return cls.add(metadata)
        elif request.method == 'DELETE' and ignored_day_id:
            return cls.remove(ignored_day_id)

    @classmethod
    def index(cls, timetable_title):
        from timetable.models.timetable import Timetable
        current_timetable = Timetable.objects.get(title=timetable_title)
        all_days = current_timetable.get_ignored_days()
       # locale.setlocale(locale.LC_ALL, 'ru_RU.UTF-8')
        for period in all_days:
            period['beginning_of_period'] = period['beginning_of_period'].strftime("%B %d, %Y")
            period['end_of_period'] = period['end_of_period'].strftime("%B %d, %Y")
        return HttpResponse(json.dumps(all_days), content_type="application/json")

    @classmethod
    def add(cls, metadata):
        ignored_period = IgnoredDay()
        ignored_period.beginning_of_period = datetime.datetime.strptime(metadata['beginning_of_period'], "%d/%m/%Y")
        ignored_period.end_of_period = datetime.datetime.strptime(metadata['end_of_period'], "%d/%m/%Y")
        ignored_period.save()

        timetable_title = metadata.get('timetable')
        from timetable.models.timetable import Timetable
        timetable = Timetable.objects.get(title=timetable_title)
        timetable.ignored_days.add(ignored_period)

        response = {'id': ignored_period.id,
                    'beginning_of_period': ignored_period.beginning_of_period.strftime("%B %d, %Y"),
                    'end_of_period': ignored_period.end_of_period.strftime("%B %d, %Y")}
        return HttpResponse(json.dumps(response), content_type="application/json")

    @classmethod
    def remove(cls, ignored_day_id):
        ignored_period = IgnoredDay.objects.get(id=ignored_day_id)
        timetable = ignored_period.timetable_set.get()
        timetable.ignored_days.remove(ignored_period)
        ignored_period.delete()
        response = []
        return HttpResponse(json.dumps(response), content_type="application/json")
