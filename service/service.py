import ast
import json
import requests
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.http import HttpResponseForbidden
from django.shortcuts import redirect as redirect_to_page

from main.settings import ACCOUNTS_SERVER


class IntegerListField(models.TextField):
    __metaclass__ = models.SubfieldBase
    description = "Stores a python list"

    def __init__(self, *args, **kwargs):
        super(IntegerListField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        if not value:
            value = []

        if isinstance(value, list):
            return value

        return ast.literal_eval(value)

    def get_prep_value(self, value):
        if value is None:
            return value

        return value


def query_set_to_dict(query_set, *args):
    """""
    Конвертирует Django-вский QuerySet в обычный словарик.
    Аргументами принимает поля по которым делается выбрка из QuerySet
    """""
    values_query_set = query_set.values(*args)
    return [entry for entry in values_query_set]


def instance_to_dict(model, *args):
    """""
    Конвертирует Django-всую Model в обычный словарик.
    Аргументами принимает поля по которым делается выбрка из Model
    """""
    model_dict = {}
    for arg in args:
        try:
            value = model.__getattribute__(arg)
        except ObjectDoesNotExist:  # Возможны неприятности с преобразованиями
            value = ''
        model_dict[arg] = value if isinstance(value, str) else value.__str__()
        # Переобразовываем в строковое представление при надобности
    return model_dict


def only_superuser(view_action):
    def a_wrapper_accepting_arguments(request, *args, **kwargs):
        if request.user and request.user.is_superuser:
            return view_action(request, *args, **kwargs)
        else:
            return HttpResponseForbidden()

    return a_wrapper_accepting_arguments


def remote_auth(view_action):
    def a_wrapper_accepting_arguments(request, *args, **kwargs):
        if request.user.id is None:
            cookie = request.COOKIES.get('sessionid')
            if cookie:
                req = requests.get('{0}api/v1/session_exist/{1}'.format(ACCOUNTS_SERVER, cookie))
                resp = json.loads(req.content.decode())
                if resp.get('session_exists'):  # Есть запись о данном пользователе
                    # Узнаем побольше о нём
                    req = requests.get('{0}api/v1/get_user_id/{1}'.format(ACCOUNTS_SERVER, cookie))
                    resp = json.loads(req.content.decode())
                    user_id = resp.get('user_id')
                    if user_id:
                        user = User.objects.get(id=user_id)
                        user.backend = 'django.contrib.auth.backends.ModelBackend'
                        login(request, user)
                        request.user = user
        if request.user.id is None:  # Авторизация не прошла
            redirect_to = '{0}://{1}{2}'.format(request.scheme, request.get_host(), request.get_full_path())

            response = redirect_to_page(ACCOUNTS_SERVER)
            response.set_cookie('redirect_to', redirect_to)
            return response
        return view_action(request, *args, **kwargs)

    return a_wrapper_accepting_arguments
