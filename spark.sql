-- MySQL dump 10.13  Distrib 5.6.25, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: spark
-- ------------------------------------------------------
-- Server version	5.6.25-4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auditories`
--

DROP TABLE IF EXISTS `auditories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auditories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auditories`
--

LOCK TABLES `auditories` WRITE;
/*!40000 ALTER TABLE `auditories` DISABLE KEYS */;
INSERT INTO `auditories` VALUES (18,1),(19,4),(20,3),(21,2),(22,5),(23,12),(24,30),(25,20),(26,8),(27,9),(28,333),(29,334),(30,7),(31,6),(32,13),(33,26),(34,11),(35,10),(36,19);
/*!40000 ALTER TABLE `auditories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
INSERT INTO `auth_group` VALUES (1,'students'),(2,'tutors');
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group__permission_id_45f1ced3212351a2_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group__permission_id_45f1ced3212351a2_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permission_group_id_2773e581295474f2_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  CONSTRAINT `auth__content_type_id_3c155ead69a12a4c_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add log entry',6,'add_logentry'),(17,'Can change log entry',6,'change_logentry'),(18,'Can delete log entry',6,'delete_logentry'),(19,'Can add Пользователь',7,'add_account'),(20,'Can change Пользователь',7,'change_account'),(21,'Can delete Пользователь',7,'delete_account'),(22,'Can add social user',8,'add_socialuser'),(23,'Can change social user',8,'change_socialuser'),(24,'Can delete social user',8,'delete_socialuser');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (6,'pbkdf2_sha256$20000$z0yXkxa2ZzMH$45edznbvNR14sPHPar5NTaaPGe4KFHXL7bSS99wdh6k=','2015-11-10 14:19:36.128373',0,'vk274309154','Виталий','Сотников','hpuos@skgfi.llh',0,1,'2015-11-08 14:24:51.837211'),(7,'pbkdf2_sha256$20000$24NRhOLjeBie$8jGIKn1JQ+hpSmMvGZOGYLRIled6aqIHxKLah+dvbjk=','2015-11-10 17:30:18.635238',1,'user','','','',1,1,'2015-11-08 14:30:40.784633'),(185,'pbkdf2_sha256$20000$nX6RgDs0ofKU$S2OliXzuwdBGQn6Yxe6Gw5q7KuEFYksBWJTc9hLI4k8=','2015-11-10 19:01:53.074101',0,'TestUser','','','xxx@xxx.xxx',1,1,'2015-11-10 19:00:25.000000');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_7b5800753ce08501_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_7b5800753ce08501_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_11eba963a6e2db22_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_u_permission_id_550e0735cccf4a20_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_u_permission_id_550e0735cccf4a20_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissi_user_id_119ca3bf21d32e01_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banned_periods_discipline`
--

DROP TABLE IF EXISTS timetables_banned_days;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banned_periods_discipline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discipline_id` int(11) NOT NULL,
  `pair_id` int(11) NOT NULL,
  `day_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `banned_periods_discipline_2a73e9e7` (`discipline_id`),
  KEY `day_id` (`day_id`),
  KEY `pair_id` (`pair_id`),
  CONSTRAINT `banned_periods__discipline_id_54b4aff0da28f267_fk_disciplines_id` FOREIGN KEY (`discipline_id`) REFERENCES `disciplines` (`id`),
  CONSTRAINT `banned_periods_discipline_ibfk_1` FOREIGN KEY (`day_id`) REFERENCES `study_days` (`id`),
  CONSTRAINT `banned_periods_discipline_ibfk_2` FOREIGN KEY (`pair_id`) REFERENCES `pairs` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banned_periods_discipline`
--

LOCK TABLES timetables_banned_days WRITE;
/*!40000 ALTER TABLE timetables_banned_days DISABLE KEYS */;
INSERT INTO timetables_banned_days VALUES (2, 15, 2, 2),(3, 16, 2, 2),(4, 17, 4, 6),(8, 8, 1, 4),(33, 8, 1, 4);
/*!40000 ALTER TABLE timetables_banned_days ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banned_periods_discipline_pair`
--

DROP TABLE IF EXISTS `banned_periods_discipline_pair`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banned_periods_discipline_pair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bannedperioddiscipline_id` int(11) NOT NULL,
  `pair_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bannedperioddiscipline_id` (`bannedperioddiscipline_id`,`pair_id`),
  KEY `banned_periods_discipline_p_pair_id_450f4a8be84960b6_fk_pairs_id` (`pair_id`),
  CONSTRAINT `banned_periods_discipline_p_pair_id_450f4a8be84960b6_fk_pairs_id` FOREIGN KEY (`pair_id`) REFERENCES `pairs` (`id`),
  CONSTRAINT `f1c96762f4fa4671cada78ffbb9f39a0` FOREIGN KEY (`bannedperioddiscipline_id`) REFERENCES timetables_banned_days (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banned_periods_discipline_pair`
--

LOCK TABLES `banned_periods_discipline_pair` WRITE;
/*!40000 ALTER TABLE `banned_periods_discipline_pair` DISABLE KEYS */;
/*!40000 ALTER TABLE `banned_periods_discipline_pair` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (1,'Первый курс'),(2,'Второй курс'),(3,'Второй курс'),(4,'Первый курс'),(5,'Первый курс'),(6,'Второй курс'),(7,'Первый курс'),(13,'Второй');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_groups`
--

DROP TABLE IF EXISTS `courses_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `studygroup_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `course_id` (`course_id`,`studygroup_id`),
  KEY `courses_groups_studygroup_id_16dd96c6b0c3b117_fk_study_groups_id` (`studygroup_id`),
  CONSTRAINT `courses_groups_course_id_39bf5ec6a61d4141_fk_courses_id` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  CONSTRAINT `courses_groups_studygroup_id_16dd96c6b0c3b117_fk_study_groups_id` FOREIGN KEY (`studygroup_id`) REFERENCES `study_groups` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_groups`
--

LOCK TABLES `courses_groups` WRITE;
/*!40000 ALTER TABLE `courses_groups` DISABLE KEYS */;
INSERT INTO `courses_groups` VALUES (1,1,1),(3,2,2),(4,3,3),(5,4,4),(6,4,5),(7,5,6),(8,6,7),(38,7,38);
/*!40000 ALTER TABLE `courses_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disciplines`
--

DROP TABLE IF EXISTS `disciplines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disciplines`
--

LOCK TABLES `disciplines` WRITE;
/*!40000 ALTER TABLE `disciplines` DISABLE KEYS */;
INSERT INTO `disciplines` VALUES (8,'Схоластика'),(9,'Астрономия'),(15,'Информатика и ИКТ'),(16,'Русский язык'),(17,'Математика'),(18,'Физика'),(19,'География'),(20,'Речь и культура общения'),(21,'Психология'),(22,'Социология'),(23,'История'),(24,'Химия'),(25,'Философия'),(26,'Ядерная физика'),(27,'Литература');
/*!40000 ALTER TABLE `disciplines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disciplines_auditories`
--

DROP TABLE IF EXISTS `disciplines_auditories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplines_auditories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discipline_id` int(11) NOT NULL,
  `auditory_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `discipline_id` (`discipline_id`,`auditory_id`),
  KEY `discipline_auditory_id_453fcbb70f42bb22_fk_timetable_auditory_id` (`auditory_id`),
  CONSTRAINT `discipline_auditory_id_453fcbb70f42bb22_fk_timetable_auditory_id` FOREIGN KEY (`auditory_id`) REFERENCES `auditories` (`id`),
  CONSTRAINT `disciplines_audi_discipline_id_d332473ebaaf166_fk_disciplines_id` FOREIGN KEY (`discipline_id`) REFERENCES `disciplines` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disciplines_auditories`
--

LOCK TABLES `disciplines_auditories` WRITE;
/*!40000 ALTER TABLE `disciplines_auditories` DISABLE KEYS */;
INSERT INTO `disciplines_auditories` VALUES (19,8,18),(16,8,19),(17,8,20),(18,8,21),(23,9,18),(21,9,20),(20,9,21),(22,9,22),(26,15,21),(24,15,23),(25,15,24),(27,15,25),(30,16,19),(28,16,22),(35,17,20),(34,17,21),(33,17,26),(36,18,20),(38,18,26),(39,20,29),(40,20,30),(42,21,21),(41,21,22),(58,22,19),(60,22,26),(61,22,34),(43,23,18),(45,23,19),(46,23,20),(44,23,22),(47,24,26),(48,24,27),(52,25,20),(51,25,21),(49,25,30),(50,25,31),(53,26,32),(54,26,33),(55,27,34),(56,27,35),(57,27,36);
/*!40000 ALTER TABLE `disciplines_auditories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disciplines_course`
--

DROP TABLE IF EXISTS `disciplines_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplines_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `duration` int(10) unsigned NOT NULL,
  `course_id` int(11) NOT NULL,
  `depend_id` int(11) DEFAULT NULL,
  `discipline_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `disciplines_course_course_id_20590b0714836273_fk_courses_id` (`course_id`),
  KEY `disciplines_cou_discipline_id_3d338ce677e81431_fk_disciplines_id` (`discipline_id`),
  KEY `disciplines_course_depend_id_464e22cc2e0c59bf_fk_disciplines_id` (`depend_id`),
  CONSTRAINT `disciplines_cou_discipline_id_3d338ce677e81431_fk_disciplines_id` FOREIGN KEY (`discipline_id`) REFERENCES `disciplines` (`id`),
  CONSTRAINT `disciplines_course_course_id_20590b0714836273_fk_courses_id` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`),
  CONSTRAINT `disciplines_course_depend_id_464e22cc2e0c59bf_fk_disciplines_id` FOREIGN KEY (`depend_id`) REFERENCES `disciplines` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disciplines_course`
--

LOCK TABLES `disciplines_course` WRITE;
/*!40000 ALTER TABLE `disciplines_course` DISABLE KEYS */;
INSERT INTO `disciplines_course` VALUES (1,1,1,NULL,18),(3,4,6,18,8),(4,3,1,NULL,9),(5,14,3,9,15),(6,11,3,16,20),(7,9,1,16,20),(8,2,1,26,21),(9,15,2,16,23),(10,20,2,21,8),(11,10,2,NULL,9),(12,12,2,NULL,18),(13,8,5,9,17),(14,8,5,18,26),(15,18,5,NULL,21),(16,16,5,NULL,22),(17,16,1,16,25);
/*!40000 ALTER TABLE `disciplines_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `disciplines_tutors`
--

DROP TABLE IF EXISTS `disciplines_tutors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disciplines_tutors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discipline_id` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `discipline_id` (`discipline_id`,`tutor_id`),
  KEY `disciplines_tutors_tutor_id_7514b58a6adc3320_fk_tutors_id` (`tutor_id`),
  CONSTRAINT `disciplines_tut_discipline_id_2cbe376c60dd8952_fk_disciplines_id` FOREIGN KEY (`discipline_id`) REFERENCES `disciplines` (`id`),
  CONSTRAINT `disciplines_tutors_tutor_id_7514b58a6adc3320_fk_tutors_id` FOREIGN KEY (`tutor_id`) REFERENCES `tutors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `disciplines_tutors`
--

LOCK TABLES `disciplines_tutors` WRITE;
/*!40000 ALTER TABLE `disciplines_tutors` DISABLE KEYS */;
INSERT INTO `disciplines_tutors` VALUES (6,8,2),(14,8,11),(36,8,30),(10,9,1),(12,9,3),(37,15,1),(38,15,3),(39,16,31),(40,16,32),(42,17,32),(41,17,33),(43,18,30),(44,19,3),(45,20,34),(46,21,33),(47,22,1),(48,23,11),(49,24,32),(50,25,32),(51,26,34),(52,27,35);
/*!40000 ALTER TABLE `disciplines_tutors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext COLLATE utf8_unicode_ci,
  `object_repr` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `djang_content_type_id_188980ca75629997_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_23a1528057a5de30_fk_auth_user_id` (`user_id`),
  CONSTRAINT `djang_content_type_id_188980ca75629997_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_23a1528057a5de30_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2015-11-10 17:31:54.470998','1','students',1,'',2,7),(2,'2015-11-10 17:32:32.512597','2','tutors',1,'',2,7),(3,'2015-11-10 19:01:48.239006','185','TestUser',2,'Изменен is_staff.',3,7);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_6aeeaabbe33cc07d_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (7,'accounts','account'),(8,'accounts','socialuser'),(6,'admin','logentry'),(2,'auth','group'),(1,'auth','permission'),(3,'auth','user'),(4,'contenttypes','contenttype'),(5,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2015-11-08 13:56:54.204195'),(2,'contenttypes','0002_remove_content_type_name','2015-11-08 13:56:54.453609'),(3,'auth','0001_initial','2015-11-08 13:56:55.574207'),(4,'auth','0002_alter_permission_name_max_length','2015-11-08 13:56:55.686891'),(5,'auth','0003_alter_user_email_max_length','2015-11-08 13:56:55.787810'),(6,'auth','0004_alter_user_username_opts','2015-11-08 13:56:55.818798'),(7,'auth','0005_alter_user_last_login_null','2015-11-08 13:56:55.962406'),(8,'auth','0006_require_contenttypes_0002','2015-11-08 13:56:55.970588'),(9,'accounts','0001_initial','2015-11-08 13:56:56.276090'),(10,'admin','0001_initial','2015-11-08 13:56:56.579117'),(11,'sessions','0001_initial','2015-11-08 13:56:56.711813');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('0clo5xhlk6jyoyyfrc32mrkb9jkldfqy','YWY5MjllZjIzMDg0MmFjNjhjNDEwNDlkODU4YjhkMjc0YWQ5NDliODp7Il9hdXRoX3VzZXJfaGFzaCI6IjUyOGFkMGRkZDIxNmJiNGI5NGU5NjEyODFiM2FhYTdmZjc0NDQ2MjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:56:02.972945'),('2tctpucz4hq04u5966e7tkl8hn1pu9zq','ZTNmZDE0MWNmN2VhOGE0YWYxYzJmMjNlZDI0MTA5NWYyMjgwNjc4ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjciLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 14:43:13.794420'),('36ldlkeroxeybadnur79phlw492fejjc','NjNhOGU3NTVjNDYzOWYzMGQxNWRlMTI0NjcxNmNhYzExNmFmM2RkNjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2015-11-24 14:33:33.899688'),('4n41s2e8846p807i7en2o08oql7mk3ik','MGM2NTdjNzFmYjZjMmUyYTBlYTllMDk1MTE0ZmUwOTVmMDE3MDgwNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjUyOGFkMGRkZDIxNmJiNGI5NGU5NjEyODFiM2FhYTdmZjc0NDQ2MjYiLCJfYXV0aF91c2VyX2lkIjoiNyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2015-11-24 14:44:01.468470'),('5y9mhoje7qf9pg7aus50vvqp8hn32fez','MGIyODAyNGY3ZWQyMDQyNDk5ZGE3YzUyYzRkMTZjOWY3ZWJmMTAxZjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 14:48:46.104099'),('65dmgc9umd1ffwed3q4lcjpvwylm66gh','ODIxOWIwNGJlNjE1YTI4ZDY0OTEwY2U1M2JlNTM3MzBmMjdjYTE4YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:19:15.574475'),('742kquq854pk4h5mc0vthb4f7egkpyih','ODIxOWIwNGJlNjE1YTI4ZDY0OTEwY2U1M2JlNTM3MzBmMjdjYTE4YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:52:58.114763'),('7ckv47d86cnlhf8w6m15grwn5gppjqes','MGM2NTdjNzFmYjZjMmUyYTBlYTllMDk1MTE0ZmUwOTVmMDE3MDgwNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjUyOGFkMGRkZDIxNmJiNGI5NGU5NjEyODFiM2FhYTdmZjc0NDQ2MjYiLCJfYXV0aF91c2VyX2lkIjoiNyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2015-11-24 14:44:01.787881'),('8a8g759ds1342s7liwz9k06rkuvx4pjk','NjNhOGU3NTVjNDYzOWYzMGQxNWRlMTI0NjcxNmNhYzExNmFmM2RkNjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2015-11-24 14:54:11.428458'),('a0vmqgj4v46pqwxz6iwhe9cs8tst27dg','YzNmN2FjNzE0NGZmZTczYWZjMjFkMzdhZjU0MjgwODRhNDVhNmUxZDp7Il9hdXRoX3VzZXJfaGFzaCI6IjU5MjZhMTdkNDQ5MmI0MTJlMjFhMTQ0OTU1NjYyYjU2NWUxYWFlNDciLCJfYXV0aF91c2VyX2lkIjoiMTg1IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiYWNjb3VudHMuYXV0aF9iYWNrZW5kcy5DdXN0b21Vc2VyTW9kZWxCYWNrZW5kIn0=','2015-11-24 19:01:53.093420'),('a2cqmdbyc4d8treoqxq7zm154r3z5tc9','MGM2NTdjNzFmYjZjMmUyYTBlYTllMDk1MTE0ZmUwOTVmMDE3MDgwNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjUyOGFkMGRkZDIxNmJiNGI5NGU5NjEyODFiM2FhYTdmZjc0NDQ2MjYiLCJfYXV0aF91c2VyX2lkIjoiNyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2015-11-24 14:44:02.146135'),('apmyb19on2je2u43n66jqeie427edlaq','MGM2NTdjNzFmYjZjMmUyYTBlYTllMDk1MTE0ZmUwOTVmMDE3MDgwNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjUyOGFkMGRkZDIxNmJiNGI5NGU5NjEyODFiM2FhYTdmZjc0NDQ2MjYiLCJfYXV0aF91c2VyX2lkIjoiNyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2015-11-24 14:07:52.151929'),('bzsqvomw0df77v137fg8dyo64v98grhq','ZTNmZDE0MWNmN2VhOGE0YWYxYzJmMjNlZDI0MTA5NWYyMjgwNjc4ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjciLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 14:43:14.094912'),('bzus4tp1ydznfv9yvdakqezkq75sy4ta','MGIyODAyNGY3ZWQyMDQyNDk5ZGE3YzUyYzRkMTZjOWY3ZWJmMTAxZjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 14:48:46.755284'),('e1npwdbej4ly8z7aome6bltwtma1q6y7','ODIxOWIwNGJlNjE1YTI4ZDY0OTEwY2U1M2JlNTM3MzBmMjdjYTE4YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:49:11.184947'),('fw9xhrltl0fec81sn1ejatz4mij02zko','YWY5MjllZjIzMDg0MmFjNjhjNDEwNDlkODU4YjhkMjc0YWQ5NDliODp7Il9hdXRoX3VzZXJfaGFzaCI6IjUyOGFkMGRkZDIxNmJiNGI5NGU5NjEyODFiM2FhYTdmZjc0NDQ2MjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:56:03.287272'),('gpq2k20zswrj8gurqyaalbylthywyt55','YWY5MjllZjIzMDg0MmFjNjhjNDEwNDlkODU4YjhkMjc0YWQ5NDliODp7Il9hdXRoX3VzZXJfaGFzaCI6IjUyOGFkMGRkZDIxNmJiNGI5NGU5NjEyODFiM2FhYTdmZjc0NDQ2MjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:45:19.295402'),('ieqg8ck9t5oflu6rg2mgt6i4wn2b6idi','YWY5MjllZjIzMDg0MmFjNjhjNDEwNDlkODU4YjhkMjc0YWQ5NDliODp7Il9hdXRoX3VzZXJfaGFzaCI6IjUyOGFkMGRkZDIxNmJiNGI5NGU5NjEyODFiM2FhYTdmZjc0NDQ2MjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:45:19.654562'),('iyewyl3z2a2lav6a8obx77yh11h97rph','ODIxOWIwNGJlNjE1YTI4ZDY0OTEwY2U1M2JlNTM3MzBmMjdjYTE4YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:49:11.599244'),('j9mx4o4q3dnbzl6hofe12czwgyenvft1','NjNhOGU3NTVjNDYzOWYzMGQxNWRlMTI0NjcxNmNhYzExNmFmM2RkNjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2015-11-24 14:54:12.345663'),('jx4jcgf4725u01051e15q93tthn71odh','ODIxOWIwNGJlNjE1YTI4ZDY0OTEwY2U1M2JlNTM3MzBmMjdjYTE4YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:52:57.382759'),('kzkk58glnuulxsfjeqamhebppchk81y5','NjNhOGU3NTVjNDYzOWYzMGQxNWRlMTI0NjcxNmNhYzExNmFmM2RkNjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2015-11-24 14:34:07.369685'),('m8ewdgefrjflx0ypzr9qkmi45jff2s6n','YWY5MjllZjIzMDg0MmFjNjhjNDEwNDlkODU4YjhkMjc0YWQ5NDliODp7Il9hdXRoX3VzZXJfaGFzaCI6IjUyOGFkMGRkZDIxNmJiNGI5NGU5NjEyODFiM2FhYTdmZjc0NDQ2MjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:56:02.558225'),('mfw209y0ow2qio59u5ilsfu0msq1kz8b','MGIyODAyNGY3ZWQyMDQyNDk5ZGE3YzUyYzRkMTZjOWY3ZWJmMTAxZjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 14:52:30.833036'),('njyvac2iyko0p0dq267txxpqm50s9afn','ODIxOWIwNGJlNjE1YTI4ZDY0OTEwY2U1M2JlNTM3MzBmMjdjYTE4YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:49:12.202348'),('qh3auevgu3ji4y175hxjjt9h7djsvgsd','MGIyODAyNGY3ZWQyMDQyNDk5ZGE3YzUyYzRkMTZjOWY3ZWJmMTAxZjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 14:37:01.926064'),('qy5c6mlbef91tskgf7hqe6txa3mmh8wx','ODIxOWIwNGJlNjE1YTI4ZDY0OTEwY2U1M2JlNTM3MzBmMjdjYTE4YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:52:57.804467'),('rh414l15ehr0ejsn6iik4adv19j3yjkt','YWY5MjllZjIzMDg0MmFjNjhjNDEwNDlkODU4YjhkMjc0YWQ5NDliODp7Il9hdXRoX3VzZXJfaGFzaCI6IjUyOGFkMGRkZDIxNmJiNGI5NGU5NjEyODFiM2FhYTdmZjc0NDQ2MjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:45:18.895687'),('tbxzbenvx8ieqjcqrkrfu676elik1abm','MGIyODAyNGY3ZWQyMDQyNDk5ZGE3YzUyYzRkMTZjOWY3ZWJmMTAxZjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 14:48:46.411152'),('uugfsbtdlgpckclaxuetw96xihtp0bkg','MGIyODAyNGY3ZWQyMDQyNDk5ZGE3YzUyYzRkMTZjOWY3ZWJmMTAxZjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 14:52:30.042367'),('v2bpnok5nhlx6eeql8jp18zmf23zeapb','MGIyODAyNGY3ZWQyMDQyNDk5ZGE3YzUyYzRkMTZjOWY3ZWJmMTAxZjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 17:30:18.663527'),('vqe34o3y1qh5arznxsum1h1v3pvnrclt','ZTNmZDE0MWNmN2VhOGE0YWYxYzJmMjNlZDI0MTA5NWYyMjgwNjc4ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjciLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 14:43:13.466124'),('w3bknt0nms4myv4vsq6zeuqxb5twchyg','YWY5MjllZjIzMDg0MmFjNjhjNDEwNDlkODU4YjhkMjc0YWQ5NDliODp7Il9hdXRoX3VzZXJfaGFzaCI6IjUyOGFkMGRkZDIxNmJiNGI5NGU5NjEyODFiM2FhYTdmZjc0NDQ2MjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:56:03.647913'),('wcmmc1m7czm5q649hxtqv5nswc0tcf80','NjNhOGU3NTVjNDYzOWYzMGQxNWRlMTI0NjcxNmNhYzExNmFmM2RkNjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2015-11-24 14:54:11.741642'),('wu8ajoynfzxaperzwh745fq1io0awywx','ODIxOWIwNGJlNjE1YTI4ZDY0OTEwY2U1M2JlNTM3MzBmMjdjYTE4YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:49:11.899480'),('xd7hjyj9r9jifygpyye4i566sccavf7u','MGIyODAyNGY3ZWQyMDQyNDk5ZGE3YzUyYzRkMTZjOWY3ZWJmMTAxZjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 14:52:30.486756'),('xoksxw3dtura2a5y6bfhu72elveormyk','NjNhOGU3NTVjNDYzOWYzMGQxNWRlMTI0NjcxNmNhYzExNmFmM2RkNjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2015-11-24 14:54:12.039160'),('xryosizb8l2yvschgemmmnasffclp98c','MGIyODAyNGY3ZWQyMDQyNDk5ZGE3YzUyYzRkMTZjOWY3ZWJmMTAxZjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 14:52:31.125836'),('xs0893fir828wtlshkbvawcup5vfqyo5','ODIxOWIwNGJlNjE1YTI4ZDY0OTEwY2U1M2JlNTM3MzBmMjdjYTE4YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiNTI4YWQwZGRkMjE2YmI0Yjk0ZTk2MTI4MWIzYWFhN2ZmNzQ0NDYyNiIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:52:58.402941'),('ym1p1ybkzocelfdk2u8yhpty82cvs3cz','MGIyODAyNGY3ZWQyMDQyNDk5ZGE3YzUyYzRkMTZjOWY3ZWJmMTAxZjp7Il9hdXRoX3VzZXJfaWQiOiI3IiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 14:48:45.704892'),('ymea5qogyaj16w4hw6g2ltlp5c4948it','ZTNmZDE0MWNmN2VhOGE0YWYxYzJmMjNlZDI0MTA5NWYyMjgwNjc4ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjciLCJfYXV0aF91c2VyX2hhc2giOiI1MjhhZDBkZGQyMTZiYjRiOTRlOTYxMjgxYjNhYWE3ZmY3NDQ0NjI2In0=','2015-11-24 14:43:14.391399'),('z4a5w09589c70ema16u6vgpgpajymak3','YWY5MjllZjIzMDg0MmFjNjhjNDEwNDlkODU4YjhkMjc0YWQ5NDliODp7Il9hdXRoX3VzZXJfaGFzaCI6IjUyOGFkMGRkZDIxNmJiNGI5NGU5NjEyODFiM2FhYTdmZjc0NDQ2MjYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI3In0=','2015-11-24 14:45:19.950530'),('z8p3q993pdl1rm7ph4pvcu28u79gd7v6','MGM2NTdjNzFmYjZjMmUyYTBlYTllMDk1MTE0ZmUwOTVmMDE3MDgwNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjUyOGFkMGRkZDIxNmJiNGI5NGU5NjEyODFiM2FhYTdmZjc0NDQ2MjYiLCJfYXV0aF91c2VyX2lkIjoiNyIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=','2015-11-24 14:44:01.126581');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ignored_days`
--

DROP TABLE IF EXISTS `ignored_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ignored_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `beginning_of_period` date NOT NULL,
  `end_of_period` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ignored_days`
--

LOCK TABLES `ignored_days` WRITE;
/*!40000 ALTER TABLE `ignored_days` DISABLE KEYS */;
INSERT INTO `ignored_days` VALUES (1,'2015-01-01','2015-01-10'),(2,'2015-03-08','2015-03-09'),(3,'2015-02-23','2015-02-24'),(4,'2015-05-22','2015-05-23'),(29,'2015-05-22','2015-05-25');
/*!40000 ALTER TABLE `ignored_days` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pairs`
--

DROP TABLE IF EXISTS `pairs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pairs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `beginning_of_period` time NOT NULL,
  `end_of_period` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pairs`
--

LOCK TABLES `pairs` WRITE;
/*!40000 ALTER TABLE `pairs` DISABLE KEYS */;
INSERT INTO `pairs` VALUES (1,'Первая пара','08:00:00','09:50:00'),(2,'Вторая пара','10:00:00','11:50:00'),(3,'Третья пара','12:00:00','13:50:00'),(4,'Четвертая пара','14:00:00','15:50:00'),(5,'Пятая пара','16:00:00','17:30:00');
/*!40000 ALTER TABLE `pairs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_users`
--

DROP TABLE IF EXISTS `social_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(72) COLLATE utf8_unicode_ci NOT NULL,
  `via` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `uid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `social_users_user_id_45d94f36cdf24370_fk_users_user_ptr_id` (`user_id`),
  CONSTRAINT `social_users_user_id_45d94f36cdf24370_fk_users_user_ptr_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_ptr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_users`
--

LOCK TABLES `social_users` WRITE;
/*!40000 ALTER TABLE `social_users` DISABLE KEYS */;
INSERT INTO `social_users` VALUES (1,'Виталий Сотников','vk','274309154',6);
/*!40000 ALTER TABLE `social_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specialities`
--

DROP TABLE IF EXISTS `specialities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specialities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specialities`
--

LOCK TABLES `specialities` WRITE;
/*!40000 ALTER TABLE `specialities` DISABLE KEYS */;
INSERT INTO `specialities` VALUES (1,'Черная и белая бухгалтерия'),(2,'Системный анализ и управление'),(3,'Электроэнергетика и электротехника'),(4,'Информационные системы и технологии');
/*!40000 ALTER TABLE `specialities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specialities_courses`
--

DROP TABLE IF EXISTS `specialities_courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specialities_courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `speciality_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `speciality_id` (`speciality_id`,`course_id`),
  KEY `timetable_spec_course_id_71d802e51be0adbc_fk_timetable_course_id` (`course_id`),
  CONSTRAINT `timetab_speciality_id_24802af661e7085_fk_timetable_speciality_id` FOREIGN KEY (`speciality_id`) REFERENCES `specialities` (`id`),
  CONSTRAINT `timetable_spec_course_id_71d802e51be0adbc_fk_timetable_course_id` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specialities_courses`
--

LOCK TABLES `specialities_courses` WRITE;
/*!40000 ALTER TABLE `specialities_courses` DISABLE KEYS */;
INSERT INTO `specialities_courses` VALUES (1,1,1),(2,1,2),(5,2,3),(6,2,4),(7,3,5),(8,3,6),(9,4,7),(10,4,13);
/*!40000 ALTER TABLE `specialities_courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `study_days`
--

DROP TABLE IF EXISTS `study_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `study_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `study_days`
--

LOCK TABLES `study_days` WRITE;
/*!40000 ALTER TABLE `study_days` DISABLE KEYS */;
INSERT INTO `study_days` VALUES (1,'Понедельник',1),(2,'Вторник',1),(3,'Среда',1),(4,'Четверг',1),(5,'Пятница',1),(6,'Суббота',1),(7,'Воскресенье',0);
/*!40000 ALTER TABLE `study_days` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `study_groups`
--

DROP TABLE IF EXISTS `study_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `study_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `study_groups`
--

LOCK TABLES `study_groups` WRITE;
/*!40000 ALTER TABLE `study_groups` DISABLE KEYS */;
INSERT INTO `study_groups` VALUES (1,'Альфа'),(2,'Бета'),(3,'Гамма'),(4,'Дельта'),(5,'Эпсилон'),(6,'Зета'),(7,'Тета'),(38,'Омега');
/*!40000 ALTER TABLE `study_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timetables`
--

DROP TABLE IF EXISTS `timetables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timetables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `beginning_of_period` date NOT NULL,
  `end_of_period` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timetables`
--

LOCK TABLES `timetables` WRITE;
/*!40000 ALTER TABLE `timetables` DISABLE KEYS */;
INSERT INTO `timetables` VALUES (1,'Test','2015-04-01','2015-09-01');
/*!40000 ALTER TABLE `timetables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timetables_banned_period_disciplines`
--

DROP TABLE IF EXISTS banned_periods_discipline;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timetables_banned_period_disciplines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timetable_id` int(11) NOT NULL,
  `bannedperioddiscipline_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `timetable_id` (`timetable_id`,`bannedperioddiscipline_id`),
  KEY `D285fce6aadfc541edce1251746c5901` (`bannedperioddiscipline_id`),
  CONSTRAINT `D285fce6aadfc541edce1251746c5901` FOREIGN KEY (`bannedperioddiscipline_id`) REFERENCES timetables_banned_days (`id`),
  CONSTRAINT `timetables_banned_timetable_id_76a644eb9095be07_fk_timetables_id` FOREIGN KEY (`timetable_id`) REFERENCES `timetables` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timetables_banned_period_disciplines`
--

LOCK TABLES banned_periods_discipline WRITE;
/*!40000 ALTER TABLE banned_periods_discipline DISABLE KEYS */;
INSERT INTO banned_periods_discipline VALUES (2, 1, 2),(3, 1, 3),(4, 1, 4),(32, 1, 33);
/*!40000 ALTER TABLE banned_periods_discipline ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timetables_disciplines`
--

DROP TABLE IF EXISTS `timetables_disciplines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timetables_disciplines` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timetable_id` int(11) NOT NULL,
  `discipline_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `timetable_id` (`timetable_id`,`discipline_id`),
  KEY `timetables_disciplines_60c87e86` (`timetable_id`),
  KEY `timetables_disciplines_e4266bb0` (`discipline_id`),
  CONSTRAINT `discipline_id_refs_id_a3e78653` FOREIGN KEY (`discipline_id`) REFERENCES `disciplines` (`id`),
  CONSTRAINT `timetable_id_refs_id_01962cf4` FOREIGN KEY (`timetable_id`) REFERENCES `timetables` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timetables_disciplines`
--

LOCK TABLES `timetables_disciplines` WRITE;
/*!40000 ALTER TABLE `timetables_disciplines` DISABLE KEYS */;
INSERT INTO `timetables_disciplines` VALUES (8,1,8),(9,1,9),(15,1,15),(16,1,16),(17,1,17),(18,1,18),(19,1,20),(20,1,21),(21,1,22),(22,1,23),(23,1,24),(24,1,25),(25,1,26),(26,1,27);
/*!40000 ALTER TABLE `timetables_disciplines` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timetables_ignored_days`
--

DROP TABLE IF EXISTS `timetables_ignored_days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timetables_ignored_days` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timetable_id` int(11) NOT NULL,
  `ignoredday_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `timetable_id` (`timetable_id`,`ignoredday_id`),
  KEY `timetables_ignor_ignoredday_id_14be40e41fb40a_fk_ignored_days_id` (`ignoredday_id`),
  CONSTRAINT `timetables_ignor_ignoredday_id_14be40e41fb40a_fk_ignored_days_id` FOREIGN KEY (`ignoredday_id`) REFERENCES `ignored_days` (`id`),
  CONSTRAINT `timetables_ignore_timetable_id_1966dddeab574887_fk_timetables_id` FOREIGN KEY (`timetable_id`) REFERENCES `timetables` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timetables_ignored_days`
--

LOCK TABLES `timetables_ignored_days` WRITE;
/*!40000 ALTER TABLE `timetables_ignored_days` DISABLE KEYS */;
INSERT INTO `timetables_ignored_days` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(29,1,29);
/*!40000 ALTER TABLE `timetables_ignored_days` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timetables_pairs`
--

DROP TABLE IF EXISTS `timetables_pairs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timetables_pairs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timetable_id` int(11) NOT NULL,
  `pair_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `timetable_id` (`timetable_id`,`pair_id`),
  KEY `timetables_pairs_pair_id_7a98215cef668dc9_fk_pairs_id` (`pair_id`),
  CONSTRAINT `timetables_pairs_pair_id_7a98215cef668dc9_fk_pairs_id` FOREIGN KEY (`pair_id`) REFERENCES `pairs` (`id`),
  CONSTRAINT `timetables_pairs_timetable_id_5844ee70ed9d2975_fk_timetables_id` FOREIGN KEY (`timetable_id`) REFERENCES `timetables` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timetables_pairs`
--

LOCK TABLES `timetables_pairs` WRITE;
/*!40000 ALTER TABLE `timetables_pairs` DISABLE KEYS */;
INSERT INTO `timetables_pairs` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4);
/*!40000 ALTER TABLE `timetables_pairs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timetables_specialities`
--

DROP TABLE IF EXISTS `timetables_specialities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timetables_specialities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timetable_id` int(11) NOT NULL,
  `speciality_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `timetable_id` (`timetable_id`,`speciality_id`),
  KEY `timeta_speciality_id_34b45a029901a350_fk_timetable_speciality_id` (`speciality_id`),
  CONSTRAINT `timeta_speciality_id_34b45a029901a350_fk_timetable_speciality_id` FOREIGN KEY (`speciality_id`) REFERENCES `specialities` (`id`),
  CONSTRAINT `timetables_specia_timetable_id_7e9c29c4f2251bc8_fk_timetables_id` FOREIGN KEY (`timetable_id`) REFERENCES `timetables` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timetables_specialities`
--

LOCK TABLES `timetables_specialities` WRITE;
/*!40000 ALTER TABLE `timetables_specialities` DISABLE KEYS */;
INSERT INTO `timetables_specialities` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4);
/*!40000 ALTER TABLE `timetables_specialities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tutors`
--

DROP TABLE IF EXISTS `tutors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tutors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `l_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `m_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tutors`
--

LOCK TABLES `tutors` WRITE;
/*!40000 ALTER TABLE `tutors` DISABLE KEYS */;
INSERT INTO `tutors` VALUES (1,'Иван','Иванов','Иванович'),(2,'Петр','Петров','Петрович'),(3,'Сидор','Сидоров','Сидорович'),(11,'Филилип','Сильвер','Аркадьевич'),(30,'Олег','Кузьмин','Иванович'),(31,'Федор','Валиченко','Игнатьевич'),(32,'Олег','Криворучко','Павлович'),(33,'Никита','Сафронов','Александрович'),(34,'Панкрат','Беляков',''),(35,'Аверьян','Карпов','');
/*!40000 ALTER TABLE `tutors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_ptr_id` int(11) NOT NULL,
  `birth_day` date NOT NULL,
  `middle_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_ptr_id`),
  CONSTRAINT `users_user_ptr_id_2dfda47d4bafd6da_fk_auth_user_id` FOREIGN KEY (`user_ptr_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (6,'1970-01-01','','m'),(185,'2015-11-11','','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-12 22:58:55
