import json

from django.db import models
from django.http import HttpResponse

from disciplines import Discipline
from study_groups.models import StudyGroup


class Course(models.Model):
    title = models.CharField(max_length=20, verbose_name='Курс')
    groups = models.ManyToManyField(StudyGroup, verbose_name="Учебные группы изучающие курс",
                                    blank=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'courses'
        app_label = 'courses'


class LearningDiscipline(models.Model):
    discipline = models.ForeignKey(Discipline, verbose_name="Название дисциплины", related_name='discipline')
    duration = models.PositiveIntegerField(verbose_name="Число часов по дисциплине")
    course = models.ForeignKey(Course, verbose_name="Курс в котором преподается дисциплина")
    depend = models.ForeignKey(Discipline, verbose_name="Зависимость от другой дисциплины", blank=True, default=None)

    def __str__(self):
        return self.discipline

    class Meta:
        app_label = 'learning_discipline'
        db_table = 'learning_discipline'


class Api:
    @classmethod
    def dispatcher_courses(cls, request):
        if request.method == 'GET':
            return cls.get(request)
        elif request.method == 'POST':
            metadata = json.loads(request.body.decode())
            return cls.add(metadata)

    @staticmethod
    def add(metadata):
        course = Course(**metadata)
        course.save()
        metadata['id'] = course.id
        return HttpResponse(json.dumps(metadata), content_type="application/json")

    @staticmethod
    def get(request):
        timetable_title = request.GET.get('timetable')
        from timetable.models import Timetable
        timetable = Timetable.objects.get(title=timetable_title)
        all_courses = timetable.get_courses()
        courses = [{'title': course.title, 'id': course.id} for course in all_courses]
        return HttpResponse(json.dumps(courses), content_type="application/json")
