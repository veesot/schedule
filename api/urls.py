from django.conf.urls import include
from django.conf.urls import url

urlpatterns = [
    url(r'^auditories/', include('auditories.urls')),
    url(r'^specialities/', include('specialities.urls')),
    url(r'^courses/', include('courses.urls')),
    url(r'^tutors/', include('tutors.urls')),
    url(r'^days/', include('days.urls')),
    url(r'^disciplines/', include('disciplines.urls')),
    url(r'^pairs/', include('pairs.urls')),
    url(r'^groups/', include('study_groups.urls')),
]
