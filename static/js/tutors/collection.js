var app = app || {};
var DisciplinesList = Backbone.Collection.extend({
    model: app.Discipline,
    url: '/api/v1/disciplines/'
});
var TutorList = Backbone.Collection.extend({
    model: app.Tutor,
    url: '/api/v1/tutors/'
});

app.Disciplines = new DisciplinesList();
app.Tutors = new TutorList();

