var app = app || {};
var firstNames = [];
var lastNames = [];
var middleNames = [];
app.DisciplineView = Backbone.View.extend({
    tagName: 'li class="view"',
    disciplineTemplate: _.template($('#discipline-template').html()),
    initialize: function () {
         this.listenTo(this.model, 'change', this.render);
    },
    render: function () {
        this.$el.html(this.disciplineTemplate(this.model.toJSON()));
        return this;
    }
});

app.TutorView = Backbone.View.extend({
    fullNameTemplate: _.template($('#full-name-tutor').html()),
    newTutorTemplate: _.template($('#new-tutor-template').html()),
    checkInput: function (parent, ignoredChild) {
        //Проверка ввода пользователя
        var children = parent.children;
        var lfm = Object();//LFM=>Last,First,Middle name
        for (var i = 0; i < children.length; ++i) {
            if (children[i] !== ignoredChild && children[i] != undefined) {//Если аттрибут заполнен
                if (children[i].value !== '') {
                    lfm[children[i].name] = children[i].value
                }
                else {
                    alert("Не заполнено поле {0}".f(children[i].placeholder));
                    break;//Если пользователь допустил пустую строку - оборвем сразу цикл
                }

            }

        }
        return lfm;
    },
    removeTutor: function (elem) {
        var tutorView = elem.currentTarget.parentNode;
        var disciplineView = tutorView.parentNode;
        var tutorId = tutorView.id;

        //После чего сотрем упоминание в колекции и на сервере.
        var discipline = app.Disciplines.get(disciplineView.id);
        var tutors = discipline.get('tutors');
        var tutor = _.findWhere(tutors, {id: parseInt(tutorId)});//Исключим из преподователей дисциплин
        discipline.save({tutor: tutor}, {
            patch: true,
            success: function () {
                //Уберем с глаз представление преподователя
                tutorView.remove();
            }
        });


    },
    addTutor: function (elem) {
        var disciplineId = elem.currentTarget.name;
        $('ul#{0}'.f(disciplineId)).prepend(this.fullNameTemplate());

    },
    createTutor: function (lfm) {
        var dfd = $.Deferred();
        app.Tutors.create(lfm, {
            wait: true,
            success: function (tutor) {
                dfd.resolve(tutor);
            },
            error: function (err) {
                // this error message for dev only
                alert('Oops!Something is breaking');
                console.log(err);
                dfd.reject(err);
            }
        });
        return dfd.promise();

    },
    saveTutor: function (elem) {
        var ignoredChild = elem.currentTarget;//Игнорируем этого потомка
        var parent = ignoredChild.parentNode;
        var lfm = this.checkInput(parent, ignoredChild);
        if (_.size(lfm) == 3) {//Мы ожидаем полноту всех полей характеризующих преподователя
            this.insertTutor(parent, lfm)
        }
    },
    insertTutor: function (parentDiscipline, lfm) {
        var tutors = app.Tutors;
        var tutorModel = tutors.where(lfm)[0];
        //Сперва проверим существует ли данный преподователь вообще
        if (!tutorModel) {
            //Как видиомо - не существует.Создадим на сервере
            var self = this;
            this.createTutor(lfm).then(function (tutorModel) {
                //После чего добавим преподователя к выбранной дисциплине
                self.updateDisciplineView(parentDiscipline, tutorModel.toJSON());
            }, function () {
                alert("failed!");
            });
        }
        else {
            this.updateDisciplineView(parentDiscipline, tutorModel.toJSON());
        }

    },
    updateDisciplineView: function (parentDiscipline, tutor) {
        //Дорисовывает внешний вид дисциплины и её преподователей
        var disciplineId = parentDiscipline.parentNode.id;
        var discipline = app.Disciplines.get(disciplineId);
        discipline.attributes.tutors.push(tutor);
        discipline.save({tutor: tutor}, {
            patch: true,
            success: function () {
                parentDiscipline.remove();//Визуально сотрем поле добавления
            }
        });

        var newTutor = this.newTutorTemplate(tutor);
        $('ul#{0}'.f(disciplineId)).prepend(newTutor);
    }
});


app.AppView = Backbone.View.extend({
    el: '#list',
    events: {
        "click #add_tutor": "addTutor",
        "click #save_tutor": "saveTutor",
        "click #remove_tutor": "removeTutor"
    },
    tutorView: new app.TutorView,
    initialize: function () {
        this.listenTo(app.Disciplines, 'add', this.addOne);
        this.listenTo(app.Disciplines, 'reset', this.addAll);
        app.Disciplines.fetch({data: {tutors: true,timetable: timetable}});
        app.Tutors.fetch(); //#TODO придумать более изящную проверку на присутствие модели на сервере
    },
    addOne: function (discipline) {
        var view = new app.DisciplineView({model: discipline});
        $('#list').append(view.render().el);
    },
    addAll: function () {
        app.Discipline.each(this.addOne, this);
    },
    addTutor: function (elem) {
        updateNames();
        this.tutorView.addTutor(elem);
        $("#first_name").autocomplete({
            source: firstNames,
            autoFocus: true
        });
        $("#last_name").autocomplete({
            source: lastNames
        });

        $("#middle_name").autocomplete({
            source: middleNames
        });
        return false;
    },

    saveTutor: function (elem) {
        this.tutorView.saveTutor(elem);
        return false;
    },
    removeTutor: function (elem) {http://localhost:8002/timetables/%D0%A0%D0%B0%D1%81%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5%20%D0%BD%D0%B0%201-%D1%8B%D0%B9%20%D1%81%D0%B5%D0%BC%D0%B5%D1%81%D1%82%D1%80/auditories/#
        this.tutorView.removeTutor(elem);
        return false;
    }
});


function updateNames() {
    firstNames = _.uniq(app.Tutors.pluck('first_name'));
    lastNames = _.uniq(app.Tutors.pluck('last_name'));
    middleNames = _.uniq(app.Tutors.pluck('middle_name'));
}
