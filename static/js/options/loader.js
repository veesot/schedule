define(['../disciplines/model', '../disciplines/collection',
    '../pairs/model', '../pairs/collection',
    '../days/model', '../days/collection',
    'model', 'collection', 'view'], function () {
    return app;
});