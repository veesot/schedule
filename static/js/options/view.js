var app = app || {};
var days;
var disciplines;
var pairs;
app.BannedDisciplineView = Backbone.View.extend({
    events: {
        "click .remove.banned-discipline": "removeBannedDiscipline"
    },
    tagName: 'li class="view"',
    bannedDisciplineTemplate: _.template($('#banned-discipline-template').html()),
    render: function () {
        this.$el.html(this.bannedDisciplineTemplate(this.model.toJSON()));
        return this;
    },
    removeBannedDiscipline: function (elem) {
        var parentElement = elem.currentTarget.parentElement;
        var children = parentElement.children;
        var discipline = children[0].textContent;
        var day = children[1].textContent.slice(5);
        var pair = children[2].textContent.slice(6);
        var bannedDiscipline = app.BannedDisciplines.where({day: day, discipline: discipline, pair: pair})[0];
        var bannedDisciplineId = bannedDiscipline.id;
        bannedDiscipline = app.BannedDisciplines.get(bannedDisciplineId);
        bannedDiscipline.destroy({
            success: function () {
                parentElement.remove();
            }
        });
        return false;
    }
});

app.IgnoredDayView = Backbone.View.extend({
    events: {
        "click .remove.ignored-period": "removeIgnoredPeriod"
    },
    tagName: 'li class="view"',
    ignoredDayTemplate: _.template($('#ignored-day-template').html()),
    initialize: function () {
        this.listenTo(this.model, 'change', this.render);
    },
    render: function () {
        this.$el.html(this.ignoredDayTemplate(this.model.toJSON()));
        return this;
    },
    removeIgnoredPeriod: function (elem) {
        var parentElement = elem.currentTarget.parentElement;
        var children = parentElement.children;
        var beginning_of_period = children[1].textContent.slice(7);
        var end_of_period = children[3].textContent.slice(10);
        var ignorePeriod = app.IgnoredDays.where({
            beginning_of_period: beginning_of_period,
            end_of_period: end_of_period
        })[0];
        var ignorePeriodId = ignorePeriod.id;
        ignorePeriod = app.IgnoredDays.get(ignorePeriodId);
        ignorePeriod.destroy({
            success: function () {
                parentElement.remove();
            }
        });
        return false;
    }
});

var bindDays = function () {
    //Привязываем дни недели к определеным полям
    app.Days.fetch({
        success: function (data) {
            days = data.pluck('title');
            $("#day").autocomplete({
                source: days
            });
        }
    });


};
var bindDisciplines = function () {
    //Привязываем наименования дисциплин
    app.Disciplines.fetch({
        data: {timetable: timetable},
        success: function (data) {
            disciplines = data.pluck('title');
            $("#discipline").autocomplete({
                source: disciplines
            });
        }
    });


};
var bindPairs = function () {
    //Привязываем названия учебных пар
    app.Pairs.fetch({
        success: function (data) {
            pairs = data.pluck('title');
            $("#pair").autocomplete({
                source: pairs
            });
        }
    });


};
app.AppView = Backbone.View.extend({
    initialize: function () {
        this.listenTo(app.BannedDisciplines, 'add', this.addOneBanned);
        this.listenTo(app.BannedDisciplines, 'reset', this.addAllBanned);
        this.listenTo(app.IgnoredDays, 'add', this.addOneIgnored);
        this.listenTo(app.IgnoredDays, 'reset', this.addAllIgnored);
        app.BannedDisciplines.fetch({data: {timetable: timetable}});
        app.IgnoredDays.fetch({data: {timetable: timetable}});
        bindDays();
        bindDisciplines();
        bindPairs();


    },
    addOneBanned: function (discipline) {
        var view = new app.BannedDisciplineView({model: discipline});
        $(view.render().el).insertAfter("#banned_period");
    },
    addAllBanned: function () {
        app.BannedDiscipline.each(this.addOneBanned, this);
    },
    addOneIgnored: function (period) {
        var view = new app.IgnoredDayView({model: period});
        $(view.render().el).insertAfter("#ignored_day");
    },
    addAllIgnored: function () {
        app.IgnoredDay.each(this.addOneIgnored, this);
    }
});
//Для отчужденых элементов
$("#add_banned_discipline").click(function (button) {
    var parent = button.currentTarget.parentElement;
    var children = parent.children;
    var day = children['day'].value;
    var discipline = children['discipline'].value;
    var pair = children['pair'].value;
    var model = {pair: pair, discipline: discipline, day: day, timetable: timetable};
    app.BannedDisciplines.create(model);
    return false;
});

//Для отчужденых элементов
$("#add_ignore_period").click(function (button) {
    var parent = button.currentTarget.parentElement;
    var children = parent.children;
    var beginning_of_period = children['beginning_of_period'].value;
    var end_of_period = children['end_of_period'].value;
    var model = {end_of_period: end_of_period, beginning_of_period: beginning_of_period, timetable: timetable};
    app.IgnoredDays.create(model);
    return false;
});
