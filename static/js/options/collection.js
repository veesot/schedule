var app = app || {};
var BannedDisciplinesList = Backbone.Collection.extend({
    model: app.BannedDiscipline,
    url: '/api/v1/days/banned/'
});
var IgnoredDayList = Backbone.Collection.extend({
    model: app.IgnoredDay,
    url: '/api/v1/days/ignored/'
});

app.BannedDisciplines = new BannedDisciplinesList();
app.IgnoredDays = new IgnoredDayList();

