var app = app || {};

app.DisciplineView = Backbone.View.extend({
    tagName: 'li class="view"',
    disciplineTemplate: _.template($('#discipline-template').html()),
    render: function () {
        this.$el.html(this.disciplineTemplate(this.model.toJSON()));
        return this;
    }
});


app.AuditoryView = Backbone.View.extend({
    tagName: 'li class="view"',
    auditoryTemplate: _.template($('#auditory_template').html()),
    newAuditoryTemplate: _.template($('#new-auditory-template').html()),
    render: function () {
        this.$el.html(this.disciplineTemplate(this.model.toJSON()));
        return this;
    },
    checkInput: function (parent) {
        //Проверка ввода пользователя
        var numberAuditory = parent.children.number.value;
        if (numberAuditory == '') {
            alert("Не заполнено поле номер");
            return ''
        }
        else {
            return {number: numberAuditory};
        }

    },
    insertAuditory: function (auditory, disciplineId) {
        var newAuditory = this.newAuditoryTemplate(auditory.toJSON());
        $('ul#{0}.discipline.view'.f(disciplineId)).prepend(newAuditory);

    },
    removeAuditoryFromDiscipline: function (elem) {
        var auditoryView = elem.currentTarget.parentNode;
        var disciplineView = auditoryView.parentNode;
        var auditoryId = auditoryView.id;

        var discipline = app.Disciplines.get(disciplineView.id);
        var auditories = discipline.get('auditories');
        var auditory = _.findWhere(auditories, {id: parseInt(auditoryId)});//Исключим из аудиторий совместимой с дисциплиной
        discipline.save({auditory: auditory}, {
            patch: true,
            success: function () {
                //Уберем с глаз представление аудитории
                auditoryView.remove();
                $.notify("Аудитория удалена", "success");
            }
        });


    },
    addAuditory: function (elem) {
        var disciplineId = elem.currentTarget.name;
        $('ul#{0}.discipline.view'.f(disciplineId)).prepend(this.auditoryTemplate());
    },
    addAuditoryToDiscipline: function (elem) {
        var ignoredChild = elem.currentTarget;//Игнорируем этого потомка
        var parent = ignoredChild.parentNode;
        var numberAuditory = this.checkInput(parent);
        if (numberAuditory) {//Пользователь указал аудиторию
            //Проверим есть ли аудитория в уже созданых ранее
            var auditories = app.Auditories;
            var auditory = auditories.where(numberAuditory)[0];
            if (!auditory) {//Если такого аудитории ещё нет - необходимо создать насервере
                auditories.create(numberAuditory);
                auditory = auditories.where(numberAuditory)[0];
            }

            //После чего добавим аудиторию к выбранной дисциплине
            var disciplineId = parent.parentNode.id;
            var discipline = app.Disciplines.get(disciplineId);
            discipline.attributes.auditories.push(auditory);
            discipline.save({auditory: auditory}, {
                patch: true,
                success: function () {
                    parent.remove();//Визуально сотрем поле добавления
                    var view = new app.AuditoryView;
                    view.insertAuditory(auditory, disciplineId);
                    $.notify("Аудитория добавлена", "success");
                }
            });
            return false;
        }
    }
})
;


app.AppView = Backbone.View.extend({
    el: '#list',
    events: {
        "click #add_auditory": "addAuditory",
        "click #save_auditory": "addAuditoryToDiscipline",
        "click #remove_auditory": "removeAuditoryFromDiscipline"
    },
    auditoryView: new app.AuditoryView,
    // шаблон для генерации блока дисциплины
    initialize: function () {
        this.listenTo(app.Disciplines, 'add', this.addOne);
        this.listenTo(app.Disciplines, 'reset', this.addAll);
        app.Auditories.fetch();
        app.Disciplines.fetch({data: {auditories: true,timetable: timetable}});
    },
    addOne: function (discipline) {
        var view = new app.DisciplineView({model: discipline});
        $('#list').append(view.render().el);
    },
    addAll: function () {
        app.Discipline.each(this.addOne, this);
    },
    addAuditory: function (elem) {
        this.auditoryView.addAuditory(elem);
        return false;
    },
    addAuditoryToDiscipline: function (elem) {
        this.auditoryView.addAuditoryToDiscipline(elem);
        return false;
    },
    removeAuditoryFromDiscipline: function (elem) {
        this.auditoryView.removeAuditoryFromDiscipline(elem);
        return false;
    }
});
