var app = app || {};
var SpecialitiesList = Backbone.Collection.extend({
    model: app.Speciality,
    url: '/api/v1/specialities/',
    updateGroupList: function (specialityId, courseId, group, action) {
        // Зная идентификаторы модели,курса и значение группы которую нам надо удалить/добавить -
        // обновим список групп у определенной модели
        var speciality = this.get(specialityId);
        var courses = speciality.get('courses');
        var course = _.findWhere(courses, {id: parseInt(courseId)});
        //Изменим состояние конкретного курса,добавив или удалив группу
        if (action === 'remove') {
            course.groups.pop({id: group.id, title: group.title});
        }
        else if (action === 'add') {
            course.groups.push({id: group.id, title: group.title});
        }
    }
});

app.Specialities = new SpecialitiesList();

