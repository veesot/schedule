var app = app || {};


app.SpecialityView = Backbone.View.extend({
    tagName: 'li class="view"',
    specialityTemplate: _.template($('#speciality-template').html()),
    initialize: function () {
        this.listenTo(this.model, 'change', this.render);
    },
    render: function () {
        this.$el.html(this.specialityTemplate(this.model.toJSON()));
        return this;
    }
});

app.AppView = Backbone.View.extend({
    el: '#list',
    events: {
        "click #add_speciality": "addSpeciality",
        "click #remove_speciality": "removeSpeciality"
    },
    initialize: function () {
        this.listenTo(app.Specialities, 'add', this.addOne);
        this.listenTo(app.Specialities, 'reset', this.addAll);
        app.Specialities.fetch({data: {timetable: timetable}});
    },
    addOne: function (speciality) {
        var view = new app.SpecialityView({model: speciality});
        $('#list').append(view.render().el);
    },
    addAll: function () {
        app.Speciality.each(this.addOne, this);
    },
    addSpeciality: function () {
        var newSpeciality = $('#new_speciality').val();
        app.Specialities.create({'title': newSpeciality,timetable: timetable});
        $.notify("Специальность добавлена", "success");
        return false;
    },
    removeSpeciality: function (button) {
        //Сперва удалим  визуально
        button.currentTarget.parentNode.remove();

        var title = button.currentTarget.attributes.name.nodeValue;
        var specialities = app.Specialities;

        var removeSpecialityId = specialities.where({'title': title})[0].id;

        if (removeSpecialityId) {
            var removeSpeciality = specialities.get(removeSpecialityId);
            removeSpeciality.destroy();
            $.notify("Специальность удалена", "success");
        }
        return false;
    }

});
