var app = app || {};

app.TimetableView = Backbone.View.extend({
    tagName: 'li class="view"',
    fullNameTemplate: _.template($('#full-name-timetable').html()),
    initialize: function () {
        this.listenTo(this, 'change', this.render);
    },
    render: function () {
        this.$el.html(this.fullNameTemplate(this.model.toJSON()));
        return this;
    }
});


app.AppView = Backbone.View.extend({
    el: '#list',
    events: {
        "click #add_timetable": "addTimetable",
        "click #save_timetable": "saveTimetable",
        "click #remove_timetable": "removeTimetable"
    },
    timetableView: new app.TimetableView,
    initialize: function () {
        this.listenTo(app.Timetables, 'add', this.addOne);
        this.listenTo(app.Timetables, 'reset', this.addAll);
        app.Timetables.fetch();
    },
    addOne: function (timetable) {
        var view = new app.TimetableView({model: timetable});
        $('#list').append(view.render().el);
    },
    addAll: function () {
        app.Timetables.each(this.addOne, this);
    },
    addTimetable: function (elem) {
        var timetable = {};
        var children = elem.currentTarget.parentNode.children;
        for (var i = 0; i < children.length; ++i) {
            if (children[i] != undefined) {//Если аттрибут заполнен
                if (children[i].value !== '') {
                    timetable[children[i].name] = children[i].value
                }
                else {
                    alert("Не заполнено поле {0}".f(children[i].placeholder));
                    break;//Если пользователь допустил пустую строку - оборвем сразу цикл
                }

            }

        }


        app.Timetables.create(timetable, {
            wait: true,
            success: function (response) {
                var model = response.toJSON();
                //redirect to disciplines
                window.location.href = window.location.origin + '/timetables/' + model.title + '/disciplines';
            },
            error: function (err) {
                console.log(err);
            }
        });
        return false;
    },

    removeTimetable: function (elem) {
        var timetableView = elem.currentTarget.parentNode;
        var model = app.Timetables.get(timetableView.id);
        model.destroy();
        timetableView.remove();
        return false;
    }
});
