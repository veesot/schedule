var app = app || {};
var firstNames;
var lastNames;
var middleNames;
var disciplines;
var auditories;

app.Tutor = Backbone.Model.extend({});
app.Discipline = Backbone.Model.extend({});
app.Auditoriy = Backbone.Model.extend({});
var DisciplinesList = Backbone.Collection.extend({
    model: app.Discipline,
    url: '/api/v1/disciplines/'
});
var TutorList = Backbone.Collection.extend({
    model: app.Tutor,
    url: '/api/v1/tutors/'
});
var AuditoriesList = Backbone.Collection.extend({
    model: app.Auditoriy,
    url: '/api/v1/auditories/'
});

app.Auditories = new AuditoriesList();
app.Disciplines = new DisciplinesList();
app.Tutors = new TutorList();

app.Tutors.fetch({
        success: function (data) {
            firstNames = data.pluck('first_name');
            lastNames = data.pluck('last_name');
            middleNames = data.pluck('middle_name');
        }
    }
);
app.Auditories.fetch({
        success: function (data) {
            auditories = data.pluck('number').toString().split(',');

        }
    }
);
app.Disciplines.fetch({
    data: {timetable: timetable},
    success: function (data) {
        disciplines = data.pluck('title');
    }
});


var dialog;
var target;

function toggleSelectGroup(groupName) {
    var hiddenGroups = $('.hide_group');
    $.each(hiddenGroups, function (key, group) {
        group.classList.remove("hide_group");
    });
    var selectGroups = $('#group_list');
    var groupsForHide = _.filter(selectGroups.children(), function (group) {
        return groupName != group.value;
    });
    if (groupName != 'all') {
        $.each(groupsForHide, function (key, group) {
            var hiddenElements = $('.' + group.value);
            $.each(hiddenElements, function (key, element) {
                element.classList.add("hide_group");
            });
        });
    }
}


$(document).ready(function () {
    $.getJSON(url, function (data) {

        var numberOfPair = data.pairs.length + 1;
        var widthTable = 1100;
        var groups;
        var groupLessons;
        var lessonsByDay;
        var td;
        var tr;
        var tbody;
        var tableBody;
        var tdWidth = String((widthTable / numberOfPair) - 20);

        //Список групп
        var selectGroups = $('#group_list');
        var option = document.createElement("option");
        option.text = "Все группы";
        option.value = "all";
        selectGroups.append(option);
        $.each(data.groups, function (key, group) {
            option = document.createElement("option");
            option.text = group;
            option.value = group;
            selectGroups.append(option);
        });
        selectGroups = document.getElementById("group_list");
        selectGroups.addEventListener("change", function () {
            toggleSelectGroup(selectGroups.value);
        });

        //заголовки
        var table = $('#timetable');
        tableBody = document.createElement('tbody');
        table.append(tableBody);
        tbody = $("tbody");
        tr = document.createElement("tr");
        tableBody.appendChild(tr);
        td = document.createElement("td");
        td.setAttribute("class", "group");
        td.setAttribute("width", tdWidth);
        tr.appendChild(td);


        $.each(data.pairs, function (key, val) {
            td = document.createElement("td");
            td.setAttribute("class", "pair");
            td.setAttribute("width", tdWidth);
            td.innerHTML = val;
            tr.appendChild(td);
        });

        var idx = 0;
        $.each(data['schedule'], function (day, lessons) {
            tr = document.createElement("tr");
            tableBody.appendChild(tr);
            td = document.createElement("td");
            td.setAttribute("class", "day");
            td.colSpan = numberOfPair;
            td.setAttribute("id", day);
            td.innerHTML = day;
            tr.appendChild(td);
            groups = getGroupByDay(lessons);
            $.each(groups, function (key, group) {
                tr = document.createElement("tr");
                tr.setAttribute("class", group);
                tr.setAttribute("day", day);
                tableBody.appendChild(tr);
                td = document.createElement("td");
                td.setAttribute("class", "group");
                td.setAttribute("width", tdWidth);
                td.innerHTML = group;
                tr.appendChild(td);
                groupLessons = getGroupLesson(group, lessons);
                lessonsByDay = collectByPair(data.pairs, groupLessons);

                $.each(lessonsByDay, function (key, lesson) {
                    tableBody.appendChild(tr);
                    td = document.createElement("td");
                    td.setAttribute("class", "drop lesson");
                    td.setAttribute("id", "drop_" + idx);
                    td.setAttribute("width", tdWidth);
                    td.setAttribute("group", group);
                    td.setAttribute("pair", data.pairs[key]);
                    td.setAttribute("day", day);
                    var div = document.createElement("div");
                    if (lesson.length > 0) {
                        div.innerHTML = lesson[0].discipline + "<br/>" + lesson[0].tutor + "<br/>" + lesson[0].auditory + '<span class="hid">' + lesson[0].idx + '</span>';

                    }
                    else {
                        div.innerHTML = " ";
                    }

                    div.setAttribute("class", "drag");
                    div.setAttribute("id", "lesson_" + idx);
                    var inputInner = document.createElement("input");
                    inputInner.setAttribute("type", "hidden");
                    inputInner.setAttribute("class", "hidden");
                    inputInner.setAttribute("name", "drop_" + idx);
                    inputInner.setAttribute("value", "lesson_" + idx);
                    td.appendChild(div);
                    td.appendChild(inputInner);
                    tr.appendChild(td);
                    idx++;
                    lessonIdx++
                });


            });
            lessonIdx = 0;
        });
        var inputTd; // where original and final positional data will be stored,
        var inputElement; // for entry into mysql database

        $('.drag').draggable({
            cursor: "move",
            appendTo: "body",
            revert: "invalid",
            opacity: 0.5,

            start: function () {
                //store the id and element of the input td when dragging div away, to use in php later
                inputTd = $(this).closest('td').find('input').attr('id');
                inputElement = $(this).closest('td').find('input');
            }
        });

        $('.drop').droppable({
            accept: ".drag",
            tolerance: "pointer",
            snap: ".drop",

            drop: function (event, ui) {
                //ui: div element being dragged
                //this: td element being dropped into
                if (this.children.length != 2) {//Промахнулись и не прикрепили
                    ui.draggable.draggable('option', 'revert', true);
                    return false;
                }
                //Сперва соберем информацию о изменяемых ячейках
                //moved -перемещаемый объект
                //replace - замещаемый
                var movedLessonId = ui.draggable.context.id;
                var movedLessonText = ui.draggable.context.innerHTML;
                var movedLessonParentId = ui.draggable.context.parentElement.id;

                var replacedLessonId = $(this).context.firstChild.id;
                var replacedLessonText = $(this).context.firstChild.innerHTML;
                var replacedLessonParentId = $(this).context.id;

                //После чего поменяем содержимое  внутри родителей этих элементов

                var movedLessonParent = $('#' + movedLessonParentId);
                movedLessonParent[0].firstChild.innerHTML = replacedLessonText;
                movedLessonParent[0].firstChild.id = replacedLessonId;
                movedLessonParent[0].firstChild.style.cssText = "position: relative;";

                var replacedLessonParent = $('#' + replacedLessonParentId);
                replacedLessonParent[0].firstChild.innerHTML = movedLessonText;
                replacedLessonParent[0].firstChild.id = movedLessonId;
                replacedLessonParent[0].firstChild.style.cssText = "position: relative;";

                //Поменяем на серверной стороне
                var firstLessonIdx;
                var secondLessonIdx;
                var firstMetaInfo;
                var secondMetaInfo;
                try {
                    firstLessonIdx = movedLessonParent[0].childNodes[0].childNodes[5].innerHTML;
                }
                catch (e) {
                    firstLessonIdx = 0;
                    secondMetaInfo = {
                        'pair': replacedLessonParent[0].attributes.pair.value,
                        'group': replacedLessonParent[0].attributes.group.value,
                        'day': replacedLessonParent[0].attributes.day.value
                    };
                }

                try {
                    secondLessonIdx = replacedLessonParent[0].childNodes[0].childNodes[5].innerHTML;
                }
                catch (e) {
                    secondLessonIdx = 0


                }
                replaceLesson(firstLessonIdx, firstMetaInfo,
                    secondLessonIdx, secondMetaInfo
                )

            }
        });

    });

    loadEditForm();
    replaceContextMenu();
});


function getGroupByDay(lessons) {
    return _.chain(lessons).pluck('group').uniq().value().sort();
}

function collectByPair(pairs, lessons) {
    var lesson;
    var lessonsByDay = [];
    $.each(pairs, function (key, pair) {
        //Попытаемся найти в списке запланированых пар то что соответствеие текущей паре
        lesson = _.filter(lessons, function (lesson) {
            return lesson.pair == pair;
        });
        lessonsByDay.push(lesson)
    });
    return lessonsByDay;

}

function getGroupLesson(group, lessons) {

    return _.filter(lessons, function (lesson) {
        return lesson.group == group;
    });
}

function replaceLesson(firstLessonId, firstMetaInfo, secondLessonId, secondMetaInfo) {
    $.ajax({
            method: "PATCH",
            url: "/api/v1/timetables/",
            data: JSON.stringify({
                firstLessonId: firstLessonId,
                firstMetaInfo: firstMetaInfo,
                secondLessonId: secondLessonId,
                secondMetaInfo: secondMetaInfo,
                timetable: timetable
            })
        })
        .done(function (msg) {
            $.notify("Информация обновлена", "success");
        });
}

var refreshTimetable = function () {
    $.ajax({
            method: "PUT",
            url: "/api/v1/timetables/",
            data: JSON.stringify({
                timetable: timetable
            })
        })
        .done(function () {
            window.location.href = window.location.origin + '/timetables/' + timetable
        });
};

var replaceContextMenu = function () {
    //http://stackoverflow.com/questions/4495626/making-custom-right-click-context-menus-for-my-web-app
    // Trigger action when the contexmenu is about to be shown
    $(document).bind("contextmenu", function (event) {
        target = event.target;
        if (target.classList.contains("ui-draggable") || target.classList.contains("ui-droppable")) {
            event.preventDefault();
            if (target.classList.contains("ui-draggable")) {
                $("#discipline").val(target.childNodes[0].data);
                var tutor = target.childNodes[2].data.split(" ");
                $("#f_name").val(tutor[0]);
                $("#l_name").val(tutor[1]);
                $("#m_name").val(tutor[2]);
                $("#auditory").val(target.childNodes[4].data);
            }
            else {
                $("#discipline").val("");
                $("#f_name").val("");
                $("#l_name").val("");
                $("#m_name").val("");
                $("#auditory").val("");
            }
            localStorage.setItem("element", target);

            // Show contextmenu
            $(".custom-menu").finish().toggle(100).

            // In the right position (the mouse)
            css({
                top: event.pageY + "px",
                left: event.pageX + "px"
            });
        }
    });


// If the document is clicked somewhere
    $(document).bind("mousedown", function (e) {

        // If the clicked element is not the menu
        if (!$(e.target).parents(".custom-menu").length > 0) {

            // Hide it
            $(".custom-menu").hide(100);
        }
    });


// If the menu element is clicked
    $(".custom-menu li").click(function () {

        // This is the triggered action name
        switch ($(this).attr("data-action")) {

            // A case for each action. Your actions here
            case "edit":
                bindSuggest();
                dialog.dialog("open");
                break;
            case "clean":
                alert("second");
                break;
        }

        // Hide it AFTER the action was triggered
        $(".custom-menu").hide(100);
    });
};

function loadEditForm() {
    $(function () {
        var form;

        function updateLesson() {

            var firstMetaInfo = {};
            var secondMetaInfo = {};

            //Подтянем значения
            var lesson = $("#discipline").val();
            var firstName = $("#f_name").val();
            var lastName = $("#l_name").val();
            var middleName = $("#m_name").val();
            var auditory = $("#auditory").val();

            if (target.classList.contains("drag")) {
                //Нужно редактировать уже наполненый
                firstMetaInfo['id'] = target.childNodes[5].innerHTML;
                var content = target.firstChild;
            }
            else {
                firstMetaInfo['id'] = 0;
                firstMetaInfo['pair'] = target.attributes.pair.value;
                firstMetaInfo['group'] = target.attributes.group.value;
                firstMetaInfo['day'] = target.attributes.day.value;
            }
            secondMetaInfo['lesson'] = lesson;
            secondMetaInfo['firstName'] = firstName;
            secondMetaInfo['lastName'] = lastName;
            secondMetaInfo['middleName'] = middleName;
            secondMetaInfo['auditory'] = auditory;
            //Отошлем изменения
            $.ajax({
                    method: "PATCH",
                    url: "/api/v1/timetables/",
                    data: JSON.stringify({
                        replace: true,
                        firstLessonId: firstMetaInfo['id'],
                        secondLessonId: 0,
                        firstMetaInfo: firstMetaInfo,
                        secondMetaInfo: secondMetaInfo,
                        timetable: timetable
                    })
                })
                .done(function (response) {
                    var tutor = response.tutor;
                    var fullNameTutor = tutor.first_name + " " + tutor.middle_name + " " + tutor.last_name;
                    var auditory = response.auditory;
                    var discipline = response.discipline;
                    if (firstMetaInfo['id'] != 0) {
                        target.childNodes[0].textContent = discipline;
                        target.childNodes[2].textContent = fullNameTutor;
                        target.childNodes[4].textContent = auditory;
                    }
                    else {
                        target.children[0].textContent = discipline;
                        var br = document.createElement("br");
                        target.children[0].appendChild(br);
                        tutor = document.createTextNode(fullNameTutor);
                        target.children[0].appendChild(tutor);
                        br = document.createElement("br");
                        target.children[0].appendChild(br);
                        auditory = document.createTextNode(auditory);
                        target.children[0].appendChild(auditory);
                        var span = document.createElement("span");
                        span.classList.add("hid");
                        span.innerHTML = response.id;
                        target.children[0].appendChild(span);

                    }

                    //content.innerHTML = lesson + "<br/>" + firstName + " " + lastName + " " + middleName + "<br/>"+auditory;
                    dialog.dialog("close");
                    $.notify("Информация обновлена", "success");
                    //Обновим внутренее содержимое
                }).fail(function (msg) {
                $.notify("Что то пошло не так :(", "error");
            });
        }

        dialog = $("#editForm").dialog({
            autoOpen: false,
            height: 490,
            width: 320,
            modal: true,
            buttons: {
                "Изменить урок": updateLesson
            }
        });

        form = dialog.find("form").on("submit", function (event) {
            event.preventDefault();

            updateLesson();
        });
    });
}

var bindSuggest = function () {
    $("#discipline").autocomplete({
        source: disciplines
    });
    $("#auditory").autocomplete({
        source: auditories
    });
    $("#f_name").autocomplete({
        source: firstNames
    });
    $("#l_name").autocomplete({
        source: lastNames
    });
    $("#m_name").autocomplete({
        source: middleNames
    });
};


var updateTimetableSettings = function () {
    var segmentDays = $('#segment_days').val();
    var complexPairInDay = $('#complex_pairs_in_day').val();
    $.ajax({
            method: "PATCH",
            url: "/api/v1/timetables/",
            data: JSON.stringify({
                segmentDays: segmentDays,
                complexPairInDay: complexPairInDay,
                timetable: timetable
            })
        })
        .done(function (msg) {
            $.notify("Настройки обновлены", "success");
        })
        .fail(function (msg) {
            $.notify("Что то пошло не так :(", "error");
        });
};