var app = app || {};
var TimetableList = Backbone.Collection.extend({
    model: app.Timetable,
    url: '/api/v1/timetables/'
});

app.Timetables = new TimetableList();

