var app = app || {};
var DayList = Backbone.Collection.extend({
    model: app.Day,
    url: '/api/v1/days/study/'
});

app.Days = new DayList();

