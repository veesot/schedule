var app = app || {};

app.DayView = Backbone.View.extend({

    events: {
        "click #change_day": "changeDay"
    },
    tagName: 'li class="view"',
    template: _.template($('#day-template').html()),
    initialize: function () {
        this.listenTo(this.model, 'change', this.render);
    },


    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    },
    changeDay: function (elem) {
        var button = elem.currentTarget;
        var dayId = button.name;
        var day = app.Days.get(dayId);
        day.save({day_id: dayId}, {
            patch: true,
            success: function (response, day) {
                if (day['is_active']){ //Если мы начинаем считать дель активным
                    button.text = 'Исключить из расписания';//То должна быть возможность исключить его из расписания
                }
                else{
                    button.text = 'Учитывать в расписании';//Соответственно наоборот
                }
                $.notify("Информация обновлена", "success");
            }
        });
        return false;
    }
});

app.AppView = Backbone.View.extend({


    el: '#list',
    initialize: function () {
        this.listenTo(app.Days, 'add', this.addOne);
        this.listenTo(app.Days, 'reset', this.addAll);
        app.Days.fetch();
    },
    addOne: function (day) {
        var view = new app.DayView({model: day});
        $('#list').append(view.render().el);
    },
    addAll: function () {
        app.Day.each(this.addOne, this);
    }

});
