var app = app || {};
var GroupList = Backbone.Collection.extend({
    model: app.Group,
    url: '/api/v1/groups/'
});

app.Groups = new GroupList();

