var app = app || {};

app.GroupView = Backbone.View.extend({
    events: {
        "click #add_group": "addGroup",
        "click #save_group": "saveGroup",
        "click #remove_group": "removeGroup"
    },
    tagName: 'li class="view"',
    specialityTemplate: _.template($('#speciality-template').html()),
    groupTemplate: _.template($('#group-template').html()),
    groupUpdateTemplate: _.template($('#group-update-template').html()),

    render: function () {
        this.$el.html(this.specialityTemplate(this.model.toJSON()));
        return this;
    },
    addGroup: function (elem) {
        var courseId = elem.currentTarget.name;
        var newGroup = this.groupTemplate();
        $('li#{0}.course'.f(courseId)).append(newGroup);
        return false;
    },
    saveGroup: function (elem) {
        //Первым делом при инициалиизации сохранения - получим ссылку на координаты события
        var view = this;
        var parent = elem.currentTarget.parentNode;
        var titleGroup = parent.children.title.value;//Наименование новой группы
        var courseId = parent.parentNode.id;//Идентифкатор курса на котором планируется группа
        var meta_info_group = {title: titleGroup, courseId: courseId};//Набор метаинформации для создания группы
        var groups = app.Groups;
        //Получим все курсы данной специальности
        groups.create(meta_info_group, {
            success: function (response) {
                var group = response.toJSON();
                var specialityId = parent.parentNode.parentNode.id;//Идентифкатор специальности на которой планируется группа
                app.Specialities.updateGroupList(specialityId, courseId, group, 'add');
                //После того как обновили коллекцию - обновиим внешний
                view.updateView(parent, courseId, group);
                $.notify("Группа добавлена", "success");
            }
        });
        return false;
    },
    updateView: function (oldView, courseId, group) {
        //Обновляем представление
        //Получим наш шаблон НОВОЙ дисциплины
        oldView.remove();
        var updateGroup = this.groupUpdateTemplate(group);
        $("li#{0}.course".f(courseId)).append(updateGroup);
        return false;

    },
    removeGroup: function (elem) {
        var parent = elem.currentTarget.parentNode;
        var groupId = parent.id;//ID удаляемой группы
        var courseId = parent.parentNode.id;//Идентифкатор курса на котором удаяляется группа
        var group = app.Groups.get(groupId);
        group.destroy({
            success: function (response) {
                var group = response.toJSON();
                var specialityId = parent.parentNode.parentNode.id;
                app.Specialities.updateGroupList(specialityId, courseId, group, 'remove');
                parent.remove();
            }
        });
        return false;
    }

});


app.AppView = Backbone.View.extend({
    initialize: function () {
        this.listenTo(app.Specialities, 'add', this.addOne);
        this.listenTo(app.Specialities, 'reset', this.addAll);
        app.Specialities.fetch({data: {courses: true, groups: true,timetable: timetable}});
        app.Groups.fetch();
    },
    addOne: function (group) {
        var view = new app.GroupView({model: group});
        $('#list').append(view.render().el);
    },
    addAll: function () {
        app.Speciality.each(this.addOne, this);
    }
});
