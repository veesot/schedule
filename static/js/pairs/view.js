var app = app || {};


app.PairView = Backbone.View.extend({
    tagName: 'li class="view"',
    specialityTemplate: _.template($('#pair-template').html()),
    initialize: function () {
        this.listenTo(this.model, 'change', this.render);
    },
    render: function () {
        this.$el.html(this.specialityTemplate(this.model.toJSON()));
        return this;
    }
});

app.AppView = Backbone.View.extend({
    el: '#list',
    events: {
        "click #add_pair": "addPair",
        "click #remove_pair": "removePair"
    },
    initialize: function () {
        this.listenTo(app.Pairs, 'add', this.addOne);
        this.listenTo(app.Pairs, 'reset', this.addAll);
        app.Pairs.fetch();
    },
    addOne: function (speciality) {
        var view = new app.PairView({model: speciality});
        $('#list').append(view.render().el);
    },
    addAll: function () {
        app.Pair.each(this.addOne, this);
    },
    addPair: function () {
        var title = $('input[name=title]').val();
        var endOfPeriod = $('input[name=end_of_period]').val();
        var beginningOfPeriod = $('input[name=beginning_of_period]').val();
        var pair = {title: title, end_of_period: endOfPeriod, beginning_of_period: beginningOfPeriod};
        pair['timetable'] =  timetable;
        app.Pairs.create(pair);
        $.notify("Учебная пара создана", "success");
        return false;
    },
    removePair: function (button) {
        var parent = button.currentTarget.parentElement;
        var title = parent.id;
        var pairId = app.Pairs.where({title: title})[0].id;
        var pair = app.Pairs.get(pairId);
        pair.destroy();
        button.currentTarget.parentNode.remove();
        $.notify("Учебная пара удалена", "success");
        return false;
    }

});
