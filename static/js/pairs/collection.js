var app = app || {};
var PairsList = Backbone.Collection.extend({
    model: app.Pair,
    url: '/api/v1/pairs/'
});

app.Pairs = new PairsList();

