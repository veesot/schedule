String.prototype.format = String.prototype.f = function () {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};
var url = window.location.origin + '/api/v1/timetables/';
var lessonIdx = 0;
try {
    var timetable = /\/timetables\/([А-ЯЁ][а-яё].+)\/\w+/.exec(decodeURIComponent(window.location.pathname))[1];
} catch (e) {
    timetable = /\/timetables\/([А-ЯЁ][а-яё].+)\//.exec(decodeURIComponent(window.location.pathname))[1];
}
//Костыль размером с самолёт.Понять регулярки для исправления
var daysUrl = timetable.indexOf('/days');
if (daysUrl != -1) {
    timetable = timetable.slice(0, daysUrl);
}

var pagesUrl = timetable.indexOf('/page');
if (pagesUrl != -1) {
    timetable = timetable.slice(0, pagesUrl);
}

try {
    url += /\/timetables\/([А-ЯЁ][а-яё].+)\/pages\/([\w-]{1,3})/.exec(window.location.pathname)[1] + '/pages/' + /\/timetables\/(\w+)\/pages\/([\w-]{1,3})/.exec(window.location.pathname)[2] + '/'
} catch (e) {
    url += timetable;
}


