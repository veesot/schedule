define([
    '../specialities/model',
    '../specialities/collection',
    '../courses/model',
    '../courses/collection',
    '../disciplines/model',
    '../disciplines/collection',
    'model',
    'collection',
    'view'
], function (model, collection, view) {
    return app;
});