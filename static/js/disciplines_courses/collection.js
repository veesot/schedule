var app = app || {};
var CoursesAndDisciplinesList = Backbone.Collection.extend({
    model: app.Speciality,
    url: '/api/v1/specialities/'
});

app.CoursesAndDisciplines = new CoursesAndDisciplinesList();
app.Disciplines = new DisciplineList();

