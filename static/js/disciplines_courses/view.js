var app = app || {};
var disciplineList;
app.SpecialityView = Backbone.View.extend({
    events: {
        "click #add_discipline": "addDiscipline",
        "click #save_discipline": "saveDiscipline",
        "click #remove_discipline": "removeDiscipline"
    },
    tagName: 'li class="view"',
    specialityTemplate: _.template($('#speciality-template').html()),
    disciplineTemplate: _.template($('#discipline-template').html()),
    disciplineNewTemplate: _.template($('#discipline-new-template').html()),

    render: function () {
        this.$el.html(this.specialityTemplate(this.model.toJSON()));
        return this;
    },
    getInput: function (parent) {
        //Проверка ввода пользователя
        var children = parent.children;

        var discipline = children.discipline.value;
        var depend = children.depend.value;
        var duration = children.duration.value;
        return {'discipline': discipline, 'depend': depend, 'duration': duration};
    },
    addDiscipline: function (elem) {
        var courseId = elem.currentTarget.name;
        var newDiscipline = this.disciplineTemplate();
        $('ul#{0}.disciplines'.f(courseId)).prepend(newDiscipline);
        $("#new_discipline").autocomplete({
            source: app.Disciplines.pluck('title')
        });
        $("#depend_discipline").autocomplete({
            source: app.Disciplines.pluck('title')
        });
        return false;

    },
    saveDiscipline: function (elem) {
        var view = this;
        var ignoredChild = elem.currentTarget;
        var parent = ignoredChild.parentNode;

        var courseNode = parent.parentNode.parentNode;
        var courseId = courseNode.id;

        var specialityId = courseNode.parentNode.id;
        var speciality = app.CoursesAndDisciplines.get(specialityId);

        var ddd = this.getInput(parent);//DDD=>Discipline,Dependency,Duration;
        var courses = speciality.get('courses'); //Получим все курсы данной специальности
        var course = _.findWhere(courses, {id: parseInt(courseId)});//Конкретный курс с его дисциплинами

        if (ddd && course) {//Если параметры валидны и курс найден
            ddd['course'] = courseId;
            speciality.save({discipline: ddd}, {//Отшлем изменения на сервер
                patch: true, success: function (response, discipline) {
                    parent.remove();//Визуально сотрем поле добавления
                    courses = _.without(courses, course); //Удалим устаревшее состояние курса
                    course.disciplines.push(discipline);//Обновим состояние курса
                    courses.push(course); //Поместим внутрь новое состояние
                    speciality.attributes.courses = courses; //Обновим состояние специальности на клиенте.
                    view.insertDiscipline(discipline, courseId);//Отрисуем новоявленую дисциплину
                }
            });

        }
        return false;


    }
    ,
    removeDiscipline: function (elem) {
        var ignoredChild = elem.currentTarget;
        var parent = ignoredChild.parentNode;
        var disciplineId = parent.id;


        var courseNode = parent.parentNode.parentNode;
        var courseId = courseNode.id;

        var specialityId = courseNode.parentNode.id;
        var speciality = app.CoursesAndDisciplines.get(specialityId);


        var courses = speciality.get('courses');
        var course = _.findWhere(courses, {id: parseInt(courseId)});
        speciality.save({removedDisciplineId: disciplineId}, {
            patch: true,
            success: function (response, discipline) {
                courses = _.without(courses, course);
                discipline['id'] = disciplineId;//После возвращения ответа от сервера - у модели уже не будет id
                course.disciplines.pop(discipline);//Обновим состояние курса
                courses.push(course); //Поместим внутрь новое состояние
                speciality.attributes.courses = courses; //Обновим состояние специальности на клиенте.
                parent.remove();//Визуально сотрем поле c дисциплиной
            }
        });
        return false;


    },
    insertDiscipline: function (discipline, courseId) {
        //Получим наш шаблон НОВОЙ дисциплины
        var newDiscipline = this.disciplineNewTemplate(discipline);
        $("ul#{0}.disciplines".f(courseId)).prepend(newDiscipline);
        return false;
    }
});


app.AppView = Backbone.View.extend({
    initialize: function () {
        this.listenTo(app.CoursesAndDisciplines, 'add', this.addOne);
        this.listenTo(app.CoursesAndDisciplines, 'reset', this.addAll);
        app.CoursesAndDisciplines.fetch({data: {courses: true, disciplines: true,timetable: timetable}});
        app.Disciplines.fetch({data: {timetable: timetable}});
    },
    addOne: function (discipline) {
        var view = new app.SpecialityView({model: discipline});
        $('#list').append(view.render().el);
    },
    addAll: function () {
        app.Speciality.each(this.addOne, this);
    }
});
