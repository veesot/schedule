var app = app || {};
var CourseList = Backbone.Collection.extend({
    model: app.Course,
    url: '/api/v1/courses/'
});

app.Courses = new CourseList();

