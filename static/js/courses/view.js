var app = app || {};

app.CourseView = Backbone.View.extend({
    events: {
        "click #add_course": "addCourse",
        "click #remove_course": "removeCourse",
        "click #save_course": "saveCourse"
    },
    tagName: 'li class="view"',
    specialityTemplate: _.template($('#speciality-template').html()),
    courseTemplate: _.template($('#course-template').html()),
    courseNewTemplate: _.template($('#new-course-template').html()),
    render: function () {
        this.$el.html(this.specialityTemplate(this.model.toJSON()));
        return this;
    },
    addCourse: function (elem) {
        var specialityId = elem.currentTarget.name;
        //Получим наш шаблон курса
        var newCourse = this.courseTemplate();
        $("ul#{0}.courses".f(specialityId)).prepend(newCourse);
        return false;
    },
    checkInput: function (parent) {
        //Проверка ввода пользователя
        var title = parent.children.course.value;
        if (title == '') {
            alert("Не заполнено поле 'Название' ");
            return ''
        }
        else {
            return {title: title};
        }

    },
    saveCourse: function (elem) {
        var view = this;
        var ignoredChild = elem.currentTarget;
        var parent = ignoredChild.parentNode;
        var specialityId = parent.parentNode.id;//Идентификатор специальности равен идентификатору родительского списка
        var titleCourse = this.checkInput(parent);
        var speciality = app.Specialities.get(specialityId);
        //Получим все курсы данной специальности
        var courses = speciality.get('courses');
        var course = _.findWhere(courses, {title: titleCourse.title});
        if (titleCourse && !course) {//Если название валидно и данного курса нет в уже существующих
            var courseCollection = app.Courses;
            courseCollection.create(titleCourse, {
                success: function (response) {
                    course = response.toJSON();
                    speciality.attributes.courses.push(response.toJSON());//Обновим атрибуты модели на клиентской стороне
                    parent.remove();//Визуально сотрем поле добавления
                    view.insertCourse(course, specialityId);//Отрисуем новый шаблон
                    speciality.save({course: course}, {patch: true});//Отшлем изменения на сервер
                    $.notify("Курс добавлен", "success");
                }
            });

        }
        return false;


    },
    removeCourse: function (elem) {
        var courseView = elem.currentTarget.parentNode;
        var courseId = courseView.id;
        var specialityId = courseView.parentNode.id;
        var speciality = app.Specialities.get(specialityId);
        var courses = speciality.get('courses');
        var course = _.findWhere(courses, {id: parseInt(courseId)});
        //Обновим курсы на клиентской стороне
        courses = _.without(courses, course);
        speciality.attributes.courses = courses;
        //Обновим на серверной стороне
        speciality.save({course: course}, {patch: true});
        courseView.remove();
        $.notify("Курс удалён", "success");
        return false;
    },
    insertCourse: function (course, specialityId) {
        //Получим наш шаблон НОВОГО курса
        var newCourse = this.courseNewTemplate(course);
        $("ul#{0}.courses".f(specialityId)).prepend(newCourse);
        return false;
    }
});


app.AppView = Backbone.View.extend({
    initialize: function () {
        this.listenTo(app.Specialities, 'add', this.addOne);
        this.listenTo(app.Specialities, 'reset', this.addAll);
        app.Specialities.fetch({data: {courses: true, timetable: timetable}});
        app.Courses.fetch({data: {timetable: timetable}});
    },
    addOne: function (discipline) {
        var view = new app.CourseView({model: discipline});
        $('#list').append(view.render().el);
    },
    addAll: function () {
        app.Course.each(this.addOne, this);
    }
});
