var app = app || {};


app.DisciplineView = Backbone.View.extend({

    tagName: 'li class="view"',


    template: _.template($('#discipline-template').html()),
    initialize: function () {
        this.listenTo(this.model, 'change', this.render);
    },


    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
        return this;
    }
});

app.AppView = Backbone.View.extend({


    el: '#list',
    // шаблон для генерации блока дисциплины
    events: {
        "click #add_discipline": "addDiscipline",
        "click #remove_discipline": "removeDiscipline"
    },
    initialize: function () {
        this.listenTo(app.Disciplines, 'add', this.addOne);
        this.listenTo(app.Disciplines, 'reset', this.addAll);
        app.Disciplines.fetch({data: {timetable: timetable}});
    },
    addOne: function (discipline) {
        var view = new app.DisciplineView({model: discipline});
        $('#list').append(view.render().el);
    },
    addAll: function () {
        app.Discipline.each(this.addOne, this);
    },
    addDiscipline: function () {
        var newDiscipline = $('#new_discipline').val();
        app.Disciplines.create({'title': newDiscipline,timetable: timetable})
        $.notify("Дисциплина добавлена", "success");
    },
    removeDiscipline: function (button) {
        //Сперва удалим  визуально(вместе с родителем)
        button.currentTarget.parentNode.remove();

        var title = button.currentTarget.attributes.name.nodeValue;//TODO:Портянка и быдлокод.Пофиксить
        var disciplines = app.Disciplines;
        var removeDisciplineId = disciplines.where({'title': title})[0].id;

        if (removeDisciplineId) {
            var removeDiscipline = disciplines.get(removeDisciplineId);
            removeDiscipline.destroy();
        }
        $.notify("Дисциплина удалена", "success");
        return false;
    }

});
