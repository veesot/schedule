var app = app || {};
var DisciplineList = Backbone.Collection.extend({
    model: app.Discipline,
    url: '/api/v1/disciplines/'
});

app.Disciplines = new DisciplineList();

