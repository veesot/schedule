# coding=utf-8
__author__ = 'veesot'


class CourseDisciplineDependency:
    """Граф зависимости дисциплин на определеном курсе"""

    def __init__(self, course):
        self.course = course
        self.vertices = []
        self.add_course_disciplines()
        self.dependencies = {}
        self.add_discipline_dependency()

    def add_course_disciplines(self):
        """"Создаем вершины в графе"""
        disciplines = self.course.learningdiscipline_set.all()  # Получаем дисциплины преподоваемые на курсе
        for discipline in disciplines:
            self.add_vertex(discipline.discipline, discipline.duration)

    def add_discipline_dependency(self):
        """"Устанавливаем зависимости"""
        disciplines = self.course.learningdiscipline_set.all()  # Получаем дисциплины преподоваемые на курсе
        for discipline in disciplines:
            if discipline.depend_id:  # Дисциплина зависит от другой
                self.add_dependency(discipline.discipline,
                                    discipline.depend)  # Установим связь между двумя дисципилинами

    def get_vertices(self):
        """Список всех дисциплин на курсе"""
        return self.vertices

    def add_vertex(self, vertex, duration):
        """Добавляем дисциплину в список вершин"""
        self.vertices.append({'discipline': vertex, 'duration': duration})

    def add_dependency(self, children, parent):
        """
            Зависимость(связь) между двумя дисциплинами
            Храним зависмости как список смежных вершин с целевой
            Пример {a:[b,c,d],b:[c,e,f],x:[y,z]}
            Пример x - дисциплина от которой зависят(parent),
            а y,z - дисциплины которые зависят(children)
        """
        if parent not in self.dependencies and children != parent:
            self.dependencies[parent] = []  # Иницируем создание новой "родительской" дисциплины
        self.dependencies[parent].append(children)  # Добавляем к дисциплине еще одну зависящую
