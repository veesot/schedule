# coding=utf-8
from django import forms

from timetable.models.timetable import Timetable


class TimetableForm(forms.ModelForm):
    class Meta:
        model = Timetable
        exclude = ('study_groups',)
        widgets = {
            'title': forms.TextInput(attrs={'placeholder': 'Введите название'}),
            'beginning_of_period': forms.TextInput(attrs={'placeholder': 'ДД/ММ/ГГГГ'}),
            'end_of_period': forms.TextInput(attrs={'placeholder': 'ДД/ММ/ГГГГ'}),
        }
