# coding=utf-8
from datetime import timedelta as td
from typing import List
from collections import namedtuple
from auditories.models import Auditory
from days.models import StudyDay
from pairs.models import Pair
from study_groups.models import StudyGroup
from timetable.graph import CourseDisciplineDependency
from timetable.models import Timetable
from tutors.models import Tutor

__author__ = 'veesot'


def get_day_template(study_groups: List[StudyGroup], pairs):
    """
        Генерация универсального шаблона на каждый учебный день
        который характеризуется шириной равной количеству обучаемых групп + 1(для хранения названия группы)
        и высотой равной количеству пар в день + 1(для хранения назавния пары)
        :type study_groups StudyGroup
        :type pairs Pair
    """
    width = len(study_groups) + 1
    height = len(pairs) + 1

    day_template = [[None] * width for _ in range(height)]

    for x in range(width - 1):  # Заполним по горизонтали,исключая первую клетку
        day_template[0][x + 1] = study_groups[x]

    for x in range(height - 1):  # Аналогично проставим отметки по вертикали
        day_template[x + 1][0] = pairs[x]

    return day_template


def get_timetable_board(study_days, study_groups, timetable):
    """
        Создаем заготовку на которой и разместится наше расписание
        В первом приближении это одноуровневый список размеры которого - равны количеству учебых дней
        :param study_days: Учебные дни
        :type timetable Timetable
        :type study_groups StudyGroup
    """

    pairs = Pair.objects.all()
    # Заготовка с днями и шаблоном на каждый день
    template = [{'date': study_day, 'events': get_day_template(study_groups, pairs)} for study_day in study_days]

    return template


def get_study_days(timetable):
    """
        Подготовка набора УЧЕБНЫХ дней из перекрестия календарных,выходных и праздничных
        :type timetable Timetable
    """
    beginning_of_period = timetable.beginning_of_period
    end_of_period = timetable.end_of_period
    delta = end_of_period - beginning_of_period  # Разлет между двумя датами.Включает в себя и неучебные дни
    days = []
    for i in range(delta.days):
        days.append(beginning_of_period + td(days=i))

    # Следующим шагом выбросим все неактивные дни недели в которые не происходит учеба
    inactive_days = StudyDay.objects.filter(is_active=False).values('title')
    # Пройдемся по списку всех неугодных дней,исключая их из уже существующего списка учебных дней
    for inactive_day in inactive_days:
        title = inactive_day['title']
        for day in days:
            if day.strftime("%A") == title:  # День нужно выкинуть из общего списка
                days.remove(day)

    # После чего требуется выкинуть праздничные и выходные(игнорируемые дни).зачастую эти дни могут не совпадать
    # с привычными выходными днями,поэтому их обрабатываем отдельно
    ignored_periods = timetable.ignored_days.all()
    # Каждый такой период ограничивается двумя днями,началом и концом.
    # Например,это могут быть периоды государственных праздников или иные периоды созданые по желанию(каникулы,карантин)
    ignored_days = []
    for ignored_period in ignored_periods:
        delta = ignored_period.end_of_period - ignored_period.beginning_of_period
        for i in range(delta.days):
            # Заполним список игнорируемых дней для конкретного периода
            ignored_days.append(ignored_period.beginning_of_period + td(days=i))

    # После того как собрали период дни которго мы ингорируем - вычеркнем эти дни из общей базы учебных дней
    days = [x for x in days if x not in ignored_days]

    return days


def get_disciplines_groups(groups):
    """
        Предоставляем структуру дисциплин по каждой группе,учитывая зависимость всех дисциплин
        :groups StudyGroup
    """
    disciplines_groups = []
    for group in groups:
        course = group.course_set.all()[0]  # У группы всего один курс
        # Граф всех дисциплин на курсе,а как следствие дисциплин группы на этом курсе
        disciplines_graph = CourseDisciplineDependency(course)
        disciplines_groups.append({'group': group, 'disciplines_graph': disciplines_graph})
    return disciplines_groups


def get_condition_groups(disciplines_groups, groups):
    """Созадаем некий слепок состояния всех групп который будем остлеживать"""
    condition_groups = {}
    for group in groups:
        course = group.course_set.all()[0]  # Группа может находиться только на одном курсе единовременно.
        course_disciplines = course.learningdiscipline_set.all()  # Набор дисциплин преподоваемых на курсе
        set_group_discipline = {}  # Набор дисциплин для группы
        for course_discipline in course_disciplines:
            set_group_discipline[course_discipline.discipline] = {'plan': course_discipline.duration, 'fact': 0}
        condition_groups[group] = set_group_discipline
    return condition_groups


def get_auditories() -> dict:
    """Список аудиторий и их состояние(По умолчнию все они свободны для проведения дисциплины)"""
    auditories = {auditory: True for auditory in Auditory.objects.all()}
    return auditories


def get_disciplines(timetable):
    """Все дисциплины участвующие в конкретном расписании"""
    disciplines = []
    for discipline in timetable.disciplines.all():
        disciplines.append({'discipline': discipline,
                            'tutors': discipline.get_tutor_id_list(),
                            'auditories': list(discipline.auditories.all())})
    return disciplines


def get_tutors() -> dict:
    """Словарик id преподователей и их состояние(По умолчнию все они свободны для проведения дисциплины)"""
    tutors = {tutor.get('id'): True for tutor in Tutor.all()}
    return tutors


def prepare(timetable):
    """
        Соберем все разнородные данные и сформируем из них структуру для будущей генерации расписания
    """

    groups = get_study_groups_by_timetable(timetable)
    prepared_data = namedtuple('PreparedData', ['study_days', 'disciplines_groups', 'condition_groups',
                                                'timetable_board', 'auditories', 'tutors', 'disciplines', 'groups'])
    prepared_data.study_days = get_study_days(timetable)
    prepared_data.disciplines_groups = get_disciplines_groups(groups)
    prepared_data.condition_groups = get_condition_groups(prepared_data.disciplines_groups, groups)
    prepared_data.timetable_board = get_timetable_board(prepared_data.study_days, groups, timetable)
    prepared_data.auditories = get_auditories()
    prepared_data.tutors = get_tutors()
    prepared_data.disciplines = get_disciplines(timetable)
    prepared_data.groups = groups

    return prepared_data


def get_study_groups_by_timetable(timetable: Timetable) -> List[StudyGroup]:
    specialities = timetable.specialities.all()
    course_set = []
    for speciality in specialities:
        courses = speciality.courses.all()
        for course in courses:
            course_set.append(course)
    group_set = []
    for course in course_set:
        groups = course.groups.all()
        for group in groups:
            group_set.append(group)
    return group_set
