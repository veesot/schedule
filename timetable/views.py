# coding=utf-8
import datetime
import requests
from django.shortcuts import render
from main.settings import ACCOUNTS_API, STUDENT_API
from pairs import Pair
from service.service import remote_auth, only_superuser
from timetable.forms import TimetableForm
from timetable.models.timetable import Timetable, Lesson


@remote_auth
def get_list_or_create_timetable(request):
    if request.method == "POST":
        pass
    elif request.method == "GET":
        timetables = Timetable.objects.all()
        user_properties = get_user_properties(str(request.user.id))

        form = TimetableForm()
        return render(request, 'timetable/index.html',
                      {'form': form, 'user_properties': user_properties, 'timetables': timetables})


def get_user_properties(user_idx: str) -> dict:
    user_properties = {}
    r = requests.get(ACCOUNTS_API + user_idx)
    groups = r.json().get('groups')
    if groups:
        students = list(filter(lambda group: group.get('name') == "students", groups))
        if students:
            r = requests.get(STUDENT_API + user_idx)
            user_properties['study_group'] = r.json().get('study_group').get('title')
    return user_properties


@only_superuser
@remote_auth
def disciplines(request, timetable_title):
    timetable = Timetable.objects.get(title=timetable_title)
    return render(request, 'timetable/disciplines.html', {'timetable': timetable})


@only_superuser
@remote_auth
def tutors(request, timetable_title):
    timetable = Timetable.objects.get(title=timetable_title)
    return render(request, 'timetable/tutors.html', {'timetable': timetable})


@only_superuser
@remote_auth
def auditories(request, timetable_title):
    timetable = Timetable.objects.get(title=timetable_title)
    return render(request, 'timetable/auditories.html', {'timetable': timetable})


@only_superuser
@remote_auth
def specialities(request, timetable_title):
    timetable = Timetable.objects.get(title=timetable_title)
    return render(request, 'timetable/speciality.html', {'timetable': timetable})


@only_superuser
@remote_auth
def courses(request, timetable_title):
    timetable = Timetable.objects.get(title=timetable_title)
    return render(request, 'timetable/courses.html', {'timetable': timetable})


@only_superuser
@remote_auth
def disciplines_courses(request, timetable_title):
    timetable = Timetable.objects.get(title=timetable_title)
    return render(request, 'timetable/disciplines_courses.html', {'timetable': timetable})


@only_superuser
@remote_auth
def groups(request, timetable_title):
    timetable = Timetable.objects.get(title=timetable_title)
    return render(request, 'timetable/groups.html', {'timetable': timetable})


@only_superuser
@remote_auth
def pairs(request, timetable_title):
    timetable = Timetable.objects.get(title=timetable_title)
    return render(request, 'timetable/pairs.html', {'timetable': timetable})


@only_superuser
@remote_auth
def timetable(request, timetable_title: str, page_number="Всё сразу"):
    timetable = Timetable.objects.get(title=timetable_title)
    return render(request, 'timetable/timetable.html', {'timetable': timetable, 'page_number': page_number})


@remote_auth
def timetable_for_group(request, timetable_title, **kwargs):
    user_properties = get_user_properties(str(request.user.id))
    group_title = user_properties.get('study_group')
    now = datetime.datetime.now()

    pairs = list(Pair.objects.all())
    count_pairs = len(pairs)
    day_in_week = 7
    timetable = []

    # Сформируем основное тело расписания
    x = 0
    while x < day_in_week:
        lessons_to_day = [''] * (count_pairs + 1)
        date = now + datetime.timedelta(x)
        lessons = Lesson.objects.filter(day__date=date).filter(group__title=group_title)
        for lesson in lessons:
            idx_of_lesson = pairs.index(lesson.pair)
            lessons_to_day[idx_of_lesson + 1] = lesson  # Вставляем со сдвигом вправо
        lessons_to_day[0] = date.strftime("%d.%m.%Y")
        timetable.append(lessons_to_day)
        x += 1

    # Расширим список пар
    pairs.insert(0, '')

    return render(request, 'timetable/timetable_for_group.html',
                  {'timetable': timetable, 'pairs': pairs, 'group_title': group_title})


@only_superuser
@remote_auth
def study_days(request, timetable_title):
    timetable = Timetable.objects.get(title=timetable_title)
    return render(request, 'timetable/days.html', {'timetable': timetable})


@only_superuser
@remote_auth
def other_days(request, timetable_title):
    timetable = Timetable.objects.get(title=timetable_title)
    return render(request, 'timetable/options.html', {'timetable': timetable})
