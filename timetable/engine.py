# coding=utf-8
import datetime
from typing import List, Dict, Tuple, Any
from .models.timetable import Api
import asjson
import operator
import xlwt
from copy import copy
from django.http import HttpResponse
from auditories.models import Auditory
from courses.models import LearningDiscipline
from days.models import StudyDay, BannedDay
from disciplines.models import Discipline
from pairs.models import Pair
from study_groups.models import StudyGroup
from timetable.models.timetable import Timetable, Day, Lesson
from timetable.prepare import prepare, get_auditories, get_tutors, get_study_groups_by_timetable
from tutors.models import Tutor

__author__ = 'veesot'


class TempLesson:
    def __init__(self, discipline, auditory, tutor):
        self.discipline = discipline
        self.auditory = auditory
        self.tutor = tutor
        self.group = None
        self.pair = None

    def __str__(self) -> str:
        return "{discipline} в аудитории {auditory},преподователь {tutor}".format(discipline=self.discipline,
                                                                                  auditory=self.auditory,
                                                                                  tutor=self.tutor)


def get_schedule(request: Any, timetable_title: str, page_number: int = 0) -> HttpResponse:
    """Возвращаем затребованое расписание
    :param page_number: Мы используем некую пагинацию разбивающую расписание по странично
    :param request: Служебный параметр джанги
    :param timetable_title: Название расписания в БД
    """
    if request.method == "GET":
        timetable = Timetable.objects.get(title=timetable_title)
        if timetable.days.all():
            try:
                if page_number is None:
                    start_day = end_day = 0
                else:
                    start_day = int(page_number) * timetable.segment_days  # Начальная точка отсчета
                    end_day = start_day + timetable.segment_days  # Окончание периода
                response = timetable.get_json_view(start_day, end_day)
            except:
                pass  # Не будем ничего ломать.Пусть сами перезагрузят страницу
                # Соберем с нуля
                # Api.refresh(timetable.title)
                # response = build_timetable(timetable)
        else:
            response = build_timetable(timetable)  # Первая сборка
    elif request.method == "DELETE":
        return Api.delete(timetable_title)
        pass

    return HttpResponse(asjson.dumps(response), content_type='application/json')


def build_timetable(timetable: Timetable):
    timetable_fill = False  # Считаем что расписание не заполнено
    prepared_data = prepare(timetable)
    pairs = Pair.objects.all()
    groups_sorted_by_percent = percents_complete(prepared_data.condition_groups, prepared_data.groups)
    all_group = range_study_groups(groups_sorted_by_percent)
    all_tutors = get_tutors()
    all_auditories = get_auditories()

    days = 0  # Потребуется  расписание на столько дней
    for study_day in prepared_data.timetable_board:
        # В каждом дне есть несколько учебных пар.
        calendar_day = Day()
        calendar_day.date = study_day['date']
        calendar_day.save()
        timetable.days.add(calendar_day)
        for pair in pairs:
            # Пара это период.И нам надо его наполнить по максимуму
            for study_group in all_group:
                # Найдем наиболее важный урок для группы
                lesson = get_lesson(study_group, prepared_data, calendar_day)
                if lesson:
                    # Свяжем данный урок с группой,днем для проведения и парой
                    lesson.pair = pair
                    lesson.save()
                    calendar_day.lessons.add(lesson)

                    # После того как добавили - нам следует отметить эти ресурсы как занятые
                    tutors = prepared_data.tutors
                    # Найдем преподователя который будет вести этот урок и отметим что теперь он занят
                    tutors[lesson.tutor] = False

                    # Также отметим что на данной паре эта аудитория тоже занята
                    auditories = prepared_data.auditories
                    auditories[lesson.auditory] = False
                    # После чего отметим что группа получила час обучения по данной дисциплине
                    prepared_data.condition_groups[study_group][lesson.discipline]['fact'] += 1

            # После того как накидаем уроков на пару - можно освобождать все занятые ресурсы
            prepared_data.auditories = copy(all_auditories)
            prepared_data.tutors = copy(all_tutors)

            # После каждой пары мы упорядочиваем группы в процентном отношении
            groups_sorted_by_percent = percents_complete(prepared_data.condition_groups, prepared_data.groups)

            # Соберем состояние завершенности
            group_complete = [x[1] == 100 for x in groups_sorted_by_percent]
            if all(group_complete):  # Полная комплектация.Пора заканчивать
                timetable_fill = True
                break
            else:
                # Отранжируем группы в новом порядке с оглядкой на процент заполнения
                all_group = range_study_groups(groups_sorted_by_percent)
        days += 1
        if timetable_fill:
            break

    start_day = end_day = 0
    response = timetable.get_json_view(start_day, end_day)
    return response


def get_lesson(group, prepared_data, study_day) -> Lesson:
    lesson = None
    disciplines_for_group = {
        key: value['plan'] - value['fact']  # Вычисляем процентую заполненость часов по предмету
        for key, value in
        prepared_data.condition_groups[group].items()
        if value['fact'] != value['plan']
        }
    ranged_disciplines_for_group = sorted(disciplines_for_group.items(), key=operator.itemgetter(1))
    disciplines = [discipline[0] for discipline in ranged_disciplines_for_group]
    for discipline in disciplines:
        tutor = check_tutors(discipline, prepared_data.tutors)
        if tutor:
            auditory = check_auditories(discipline, prepared_data.auditories)
            if auditory:
                if check_constrains(discipline, group, disciplines_for_group, study_day):
                    lesson = Lesson()
                    lesson.discipline = discipline
                    lesson.auditory = auditory
                    lesson.tutor = tutor
                    lesson.group = group
                    return lesson
    return lesson


def check_dependency_disciplines(discipline: Discipline, group: StudyGroup,
                                 condition_group: Dict[StudyGroup, Dict[str, int]]) -> bool:
    """мы не можем преподавать определеные уроки раньше других
    :param group: Учебная группа
    :param condition_group: Группа по которой будет проводиться дисциплина
    :param discipline:Дисциплина котороую мы будем проверять на важность
    """
    # Найдем изучаемую дисциплину с привязкой к её зависимостям
    learning_discipline = LearningDiscipline.objects.get(course__groups=group,
                                                         discipline_id=discipline.id)
    if not learning_discipline.depend_id:  # Нет зависимости на другие дисциплины
        return True

    # Посмотрим состояние завершености по дисциплины от которой зависим
    dependency_discipline = Discipline.objects.get(id=learning_discipline.depend_id)
    state_dependency_discipline = condition_group.get(dependency_discipline, 0)
    if state_dependency_discipline != 0:
        return False
    else:
        return True


def check_complex_disciplines(discipline: Discipline, study_group, study_day) -> bool:
    # Поверяем - не слишком ли много данной дисциплины у группы
    """
    :param study_day:Календарный день в который происходит действие
    :param group: Учебная группа
    """
    сomplex_lesson = [lesson for lesson in study_day.lessons.all() if
                      lesson.group == study_group and lesson.discipline == discipline]
    max_lesson = Timetable.objects.get(days=study_day).complex_pairs_in_day
    if len(сomplex_lesson) < max_lesson:
        return True
    else:
        return False


def is_forbidden_lesson(discipline, study_day):
    """Проеверяем - можно ли вообще проводить урок в этот день
    :param study_day: календарный день
    """
    day_week = study_day.date.strftime("%A")
    study_day = StudyDay.objects.get(title=day_week)
    lesson_forbidden = bool(BannedDay.objects.filter(day=study_day, discipline=discipline))
    return lesson_forbidden


def check_constrains(discipline: Discipline, group: StudyGroup,
                     condition_group: Dict[StudyGroup, Dict[str, int]], study_day) -> bool:
    """Проверяем урок-кандидат на всевозможные ограничения.
    мы не можем преподавать определеные уроки раньше других
    :param group: Учебная группа
    :param condition_group: Группа по которой будет проводиться дисциплина
    :param discipline:Дисциплина котороую мы будем проверять на важность
    """
    if check_dependency_disciplines(discipline, group, condition_group):
        if check_complex_disciplines(discipline, group, study_day):
            if not is_forbidden_lesson(discipline, study_day):
                return True
    return False


def check_discipline(discipline: Discipline, tutors: Dict[Tutor, bool], auditories: Dict[Auditory, bool]) -> TempLesson:
    # Учителя нам важны в первую очередь,ибо кадры решают всё
    free_tutors = [tutor for tutor, free in tutors.items() if free]
    tutor = check_tutors(discipline, free_tutors)
    if tutor:
        # Потом нам важно место проведения
        free_auditories = [auditory for auditory, free in auditories.items() if free]
        auditory = check_auditories(discipline, free_auditories)
        if auditory:
            # Вернем все необходимые данные
            lesson = TempLesson(discipline=discipline, tutor=tutor, auditory=auditory)
            return lesson
    return None


def check_tutors(discipline: Discipline, free_tutors: List[Tutor]) -> Tutor:
    # У нас может быть от одного до нескольких преподователей которые могут вести данную дисциплину
    potential_tutors = discipline.get_tutor_id_list()
    for potential_tutor in potential_tutors:
        if potential_tutor in free_tutors and free_tutors[potential_tutor] == True:
            # Есть свободный преподователь
            return potential_tutor
    return None


def check_auditories(discipline: Discipline, free_auditories: List[Auditory]) -> Auditory:
    # Дисциплина может проводиться в нескольких аудиториях
    potential_auditories = discipline.auditories.all()
    for potential_auditory in potential_auditories:
        if potential_auditory in free_auditories and free_auditories[potential_auditory] == True:
            # Есть свободная аудитория
            return potential_auditory
    return None


def range_study_groups(groups_sorted_by_percent) -> List[StudyGroup]:
    study_groups = [x[0] for x in groups_sorted_by_percent]
    return study_groups


def percents_complete(condition_groups, study_groups) -> List[Tuple[StudyGroup, int]]:
    percents_by_group = {}  # Завершеность в процентах по расписаным часам
    for study_group in study_groups:
        condition_group = condition_groups[study_group]
        percents_by_group[study_group] = count_complete_by_group(condition_group)
    groups_sorted_by_percent = sorted(percents_by_group.items(), key=operator.itemgetter(1))
    return groups_sorted_by_percent


def count_complete_by_group(condition_group: Dict[StudyGroup, Dict[str, int]]) -> int:
    """
    Считаем завершеность комплектации учебными часами.
    Если у группы проставлены все часы - комплектация равна 100
    :param condition_group:Состояние группы отражающее количество часов по различным дисциплинам
    """
    hours_plan = sum([hours['plan'] for hours in condition_group.values()])
    hours_fact = sum([hours['fact'] for hours in condition_group.values()])
    if hours_plan != 0:  # У группы вообще есть плановые часы
        return hours_fact * 100.0 / hours_plan
    else:
        return 100  # Считаем что комплектация полная


def export_to_excel(request, timetable_title):
    filename = "Расписание {0}".format(timetable_title)
    all_tutors = Tutor.all()

    tutors = {}
    for tutor in all_tutors:
        tutors[tutor['id']] = '{0} {1} {2}'.format(tutor['last_name'], tutor['first_name'], tutor['middle_name'])
    timetable = Timetable.objects.get(title=timetable_title)
    pairs = [pair.title for pair in timetable.pairs.all()]
    days = list(timetable.days.all())
    borders = xlwt.Borders()
    borders.left = xlwt.Borders.THIN
    borders.right = xlwt.Borders.THIN
    borders.top = xlwt.Borders.DOTTED
    borders.bottom = xlwt.Borders.DOTTED
    day_style = xlwt.easyxf(
        'pattern: pattern solid, fore_colour teal;border: top thin, right thin, bottom thin, left thin;font: color white;align: horiz center')
    header_style = day_style
    group_style = xlwt.easyxf(
        'pattern: pattern solid, fore_colour pale_blue;border: top thin, right thin, bottom thin, left thin;align: horiz center')
    lesson_style = xlwt.easyxf(
        'pattern: pattern solid, fore_colour white;border: top thin, right thin, bottom thin, left thin;align: horiz center')
    # period = timetable.segment_days
    book = xlwt.Workbook()
    sheet = book.add_sheet("Расписание")

    row_number = 0
    row = sheet.row(row_number)
    row.write(0, "Группа", header_style)
    for index, pair in enumerate(pairs):
        row.write(index + 1, pair, header_style)
        col = sheet.col(index + 1)
        col.width = 500 * 20
    row.height_mismatch = True
    row.height = 20 * 30
    row_number += 1

    for day in days:
        # Заголовок даты
        sheet.write_merge(row_number, row_number, 0, len(pairs), datetime.datetime.strftime(day.date, "%d.%m.%Y"),
                          day_style)
        row_number += 1

        lessons = day.lessons.all()
        study_groups = [study_group.title for study_group in get_study_groups_by_timetable(timetable)]
        study_groups.sort()
        for study_group in study_groups:
            study_lessons = [lesson for lesson in lessons if lesson.group.title == study_group]
            row = sheet.row(row_number)
            if study_lessons:
                row.write(0, study_group, group_style)
                local_pairs = list(pairs)
                for lesson in study_lessons:
                    pair = lesson.pair
                    pair_ptr = pairs.index(pair.title)
                    row.write(pair_ptr + 1,
                              lesson.discipline.title + '\n' + tutors[lesson.tutor] + '\n' + str(
                                  lesson.auditory.number), lesson_style)
                    row.height_mismatch = True
                    row.height = 20 * 40
                    local_pairs.remove(pair.title)
                # Дорисовываем пропущеные
                for local_pair in local_pairs:
                    pair_ptr = pairs.index(local_pair)
                    row.write(pair_ptr + 1, "", lesson_style)
                row_number += 1
    book.save(filename)
    fsock = open(filename, "rb")
    response = HttpResponse(fsock)
    start_day = datetime.datetime.strftime(days[0].date, "%d.%m.%Y")
    end_day = datetime.datetime.strftime(days[-1].date, "%d.%m.%Y")
    response['Content-Disposition'] = 'attachment; filename=' + "Raspisanie_{0}-{1}.xls".format(start_day, end_day)
    response['Content-Type'] = 'application/vnd.ms-excel; charset=utf-8'
    return response
