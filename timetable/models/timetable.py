import datetime
import json
from typing import List
import asjson
import collections
from copy import copy
from django.db import models
from django.http import HttpResponse
from auditories import Auditory
from courses import Course
from days.models import StudyDay, IgnoredDay, BannedDay
from disciplines.models import Discipline
from pairs.models import Pair
from service.service import query_set_to_dict
from specialities.models import Speciality
from study_groups import StudyGroup
from tutors import Tutor


class Lesson(models.Model):
    discipline = models.ForeignKey(Discipline)
    auditory = models.ForeignKey(Auditory)
    tutor = models.IntegerField()
    group = models.ForeignKey(StudyGroup)
    pair = models.ForeignKey(Pair)

    class Meta:
        db_table = 'lessons'
        verbose_name = 'Урок'
        verbose_name_plural = "Уроки"


class Day(models.Model):
    date = models.DateField(verbose_name="Календарный день")
    lessons = models.ManyToManyField(Lesson, verbose_name="Учебные пары", blank=True)

    class Meta:
        db_table = 'days'
        verbose_name = 'Календарный день'
        verbose_name_plural = "Календарные дни"


class Timetable(models.Model):
    title = models.CharField(max_length=50, verbose_name='Наименование расписания')
    beginning_of_period = models.DateField(verbose_name='Дата начала действия')
    end_of_period = models.DateField(verbose_name='Дата окончания действия')
    disciplines = models.ManyToManyField(Discipline, verbose_name="Учебные дисциплины участвующие в расписании",
                                         blank=True)
    specialities = models.ManyToManyField(Speciality, verbose_name="Учебные специальности участвующие в расписании",
                                          blank=True)
    pairs = models.ManyToManyField(Pair, verbose_name="Расписание звонков", blank=True)
    banned_days = models.ManyToManyField(BannedDay,
                                         verbose_name="Запрет дисциплинам участвовать в определеный день "
                                                      "и время", blank=True)
    ignored_days = models.ManyToManyField(IgnoredDay,
                                          verbose_name="Праздники и прочие дни "
                                                       "которые мы хотим игнорировать при формированиии", blank=True)
    segment_days = models.IntegerField(verbose_name='Дней на страницу', default=7)
    complex_pairs_in_day = models.IntegerField(verbose_name='Связаных пар в день', default=2)
    days = models.ManyToManyField(Day, verbose_name="Календарные дни", blank=True)

    def get_specialities_dict(self):
        values_query_set = self.specialities.all().values('title', 'id')
        return [entry for entry in values_query_set]

    def get_courses(self) -> List[Course]:
        course_list = []
        all_specialities = self.specialities.all()
        for speciaity in all_specialities:
            courses = speciaity.courses.all()
            for course in courses:
                course_list.append(course)
        return course_list

    def get_courses_for_specialities(self):
        all_specialities = self.get_specialities_dict()
        for speciality in all_specialities:
            speciality['courses'] = []
            courses = Speciality.objects.get(id=speciality['id']).courses.all()
            for course in courses:
                speciality['courses'].append({'title': course.title, 'id': course.id})
        return [entry for entry in all_specialities]

    def get_pairs(self):
        values_query_set = self.pairs.all().values('title', 'beginning_of_period', 'end_of_period')
        return [entry for entry in values_query_set]

    def get_index_banned_disciplines(self):
        values_query_set = self.banned_days.all().values('discipline', 'pair', 'day', 'id')
        for entry in values_query_set:
            entry['discipline'] = Discipline.objects.get(id=entry['discipline']).title
            entry['pair'] = Pair.objects.get(id=entry['pair']).title
            entry['day'] = StudyDay.objects.get(id=entry['day']).title
        return [entry for entry in values_query_set]

    def get_ignored_days(self):
        values_query_set = self.ignored_days.all().values('beginning_of_period', 'end_of_period', 'id')
        return [entry for entry in values_query_set]

    def get_json_view(self, start_date, end_date):
        all_days = self.days.all()
        if start_date >= 0 and end_date:  # Нужен срез по дням
            all_days = all_days[start_date:end_date]  # Мы отдаем уроки только в заданом периоде
        group_ids = set([x[0] for x in all_days.values_list('lessons__auditory__lesson__group')])
        pairs = [pair.title for pair in self.pairs.all()]
        actual_groups = [StudyGroup.objects.get(id=idx).title for idx in group_ids if not (idx is None)]
        schedule = {}
        tutors = Tutor.all()  # Сразу подготовим список преподователей,дабы не ходить по сети за ним каждый раз
        for study_day in all_days:
            dt = study_day.date.strftime('%d.%m.%Y')
            schedule[dt] = []
            for lesson in study_day.lessons.all():
                group_title = lesson.group.__str__()
                # Получим информацию от удаленого API
                tutor_dict = next(filter(lambda tutor: tutor.get('id') == lesson.tutor, tutors))
                tutor = "{last_name} {first_name} {middle_name}".format(
                    first_name=tutor_dict.get('first_name'),
                    last_name=tutor_dict.get('last_name'),
                    middle_name=tutor_dict.get('middle_name')
                )

                schedule[dt].append({'discipline': lesson.discipline.__str__(),
                                     'auditory': lesson.auditory.__str__(),
                                     'group': group_title,
                                     'pair': lesson.pair.__str__(),
                                     'tutor': tutor,
                                     'idx': lesson.id})
        schedule = collections.OrderedDict(
            sorted(schedule.items(), key=lambda t: datetime.datetime.strptime(t[0], '%d.%m.%Y')))
        return {'pairs': pairs, 'schedule': schedule, 'groups': actual_groups}

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'timetables'
        app_label = 'timetable'
        verbose_name = 'Расписание'
        verbose_name_plural = "Расписания"


class Api:
    @classmethod
    def dispatcher_timetables(cls, request):
        if request.method == 'GET':
            return cls.get()
        elif request.method == 'POST':
            metadata = json.loads(request.body.decode())
            if metadata.get('title'):
                title = metadata.get('title', '')
                beginning_of_period = metadata.get('beginning_of_period', '')
                end_of_period = metadata.get('end_of_period', '')
                # Компьютеро-понимаемый формат даты
                canonic_beginning_of_period = datetime.datetime.strptime(beginning_of_period, "%d/%m/%Y")
                canonic_end_of_period = datetime.datetime.strptime(end_of_period, "%d/%m/%Y")
                # Начнем создавать заготовку расписания
                timetable = Timetable()
                timetable.beginning_of_period = canonic_beginning_of_period
                timetable.end_of_period = canonic_end_of_period
                timetable.title = title
                timetable.save()
                metadata['id'] = timetable.id
                # После создания заготовки - отправляем на новую страницу
                # для заполнения дисциплин которые преподают в учебном заведении
                return HttpResponse(json.dumps(metadata), content_type="application/json")
            return cls.add(metadata)
        elif request.method == 'PATCH':
            metadata = json.loads(request.body.decode())
            if 'segmentDays' in metadata:
                return cls.update_setting(metadata)
            return cls.update(metadata)
        elif request.method == 'PUT':
            metadata = json.loads(request.body.decode())
            timetable_title = metadata.get('timetable')
            return cls.refresh(timetable_title)
        elif request.method == 'DELETE':
            metadata = json.loads(request.body.decode())
            timetable_title = metadata.get('timetable_id')
            return cls.delete(timetable_title)

    @staticmethod
    def add(metadata):
        timetable = Timetable.objects.filter(title=metadata['title'])
        if not timetable.exists():
            title = metadata.get('title', '')
            beginning_of_period = metadata.get('beginning_of_period', '')
            end_of_period = metadata.get('end_of_period', '')
            segment_days = int(metadata.get('segment_days', ''))
            # Компьютеро-понимаемый формат даты
            canonic_beginning_of_period = datetime.datetime.strptime(beginning_of_period, "%d/%m/%Y")
            canonic_end_of_period = datetime.datetime.strptime(end_of_period, "%d/%m/%Y")
            # Начнем создавать заготовку расписания
            timetable = Timetable()
            timetable.beginning_of_period = canonic_beginning_of_period
            timetable.end_of_period = canonic_end_of_period
            timetable.title = title
            timetable.segment_days = segment_days
            timetable.save()
        else:
            timetable = Timetable.objects.get(title=metadata['title'])

        metadata['id'] = timetable.id
        return HttpResponse(json.dumps(metadata), content_type="application/json")

    @classmethod
    def get(cls):
        all_timetables = Timetable.objects.all()
        timetables = query_set_to_dict(all_timetables, 'title', 'id', 'beginning_of_period', 'end_of_period')
        return HttpResponse(asjson.dumps(timetables), content_type="application/json")

    @classmethod
    def update(cls, metadata):
        # Найдем дни в которых расположены уроки
        first_idx = int(metadata.get('firstLessonId', 0))
        second_idx = int(metadata.get('secondLessonId', 0))
        replace = metadata.get('replace', False)
        if first_idx and second_idx:
            # Полноценная замена одного на другое
            first_lesson = Lesson.objects.get(id=first_idx)
            first_day = first_lesson.day_set.all()[0]

            second_lesson = Lesson.objects.get(id=second_idx)
            second_day = second_lesson.day_set.all()[0]

            temp = copy(first_lesson)

            # Очистим старые отметки
            first_day.lessons.remove(first_lesson)
            second_day.lessons.remove(second_lesson)

            # Начнем шаманство с парами.После перемещения меняются пары и группы.
            first_lesson.group = second_lesson.group
            first_lesson.pair = second_lesson.pair
            first_lesson.save()

            second_lesson.group = temp.group
            second_lesson.pair = temp.pair
            second_lesson.save()

            first_day.lessons.add(second_lesson)
            second_day.lessons.add(first_lesson)
        elif not first_idx and second_idx and not replace:  # Перемещаем на пустую клеточку уже существующее расписание
            metainfo = metadata.get('secondMetaInfo')
            group = StudyGroup.objects.get(title=metainfo.get('group'))
            pair = Pair.objects.get(title=metainfo.get('pair'))

            second_lesson = Lesson.objects.get(id=second_idx)
            second_day = second_lesson.day_set.get()
            second_day.lessons.remove(second_lesson)
            # Заменим содержимое
            second_lesson.group = group
            second_lesson.pair = pair
            second_lesson.save()
            # Воткнем замену
            current_schedule = second_day.timetable_set.get()
            date = datetime.datetime.strptime(metainfo.get('day'), "%d.%m.%Y")
            day = Day.objects.get(date=date, timetable=current_schedule)
            day.lessons.add(second_lesson)

        elif replace:
            second_meta_info = metadata.get('secondMetaInfo')
            about_lesson = {}
            if first_idx:  # Известен урок который надо заменить
                lesson = Lesson.objects.get(id=first_idx)
            else:  # Нужно создать новый урок
                about_lesson = metadata.get('firstMetaInfo')
                lesson = Lesson()
                lesson.group = StudyGroup.objects.get(title=about_lesson.get('group'))
                lesson.pair = Pair.objects.get(title=about_lesson.get('pair'))

            l_name = second_meta_info.get('firstName')
            f_name = second_meta_info.get('lastName')
            m_name = second_meta_info.get('middleName')
            tutors = Tutor.all()
            tutor = [tutor for tutor in
                     tutors if tutor.get('first_name') == f_name
                     and tutor.get('last_name') == l_name
                     and tutor.get('middle_name') == m_name][0]
            lesson.auditory = Auditory.objects.get(number=second_meta_info.get('auditory'))
            lesson.discipline = Discipline.objects.get(title=second_meta_info.get('lesson'))
            lesson.tutor = tutor['id']
            lesson.save()

            if not second_idx and not first_idx:
                # Привяжем урок к определеному дню
                date = datetime.datetime.strptime(about_lesson.get('day'), "%d.%m.%Y")
                day = Day.objects.get(date=date, timetable__title=metadata.get('timetable'))
                day.lessons.add(lesson)
            return HttpResponse(asjson.dumps({'id': lesson.id,
                                              'tutor': tutor,
                                              'discipline': lesson.discipline.title,
                                              'auditory': lesson.auditory.number}),
                                content_type="application/json")
        return HttpResponse(asjson.dumps({}), content_type="application/json")

    @classmethod
    def refresh(cls, timetable_title):
        """Обновление расписания.Сбрасываем все наработки

        """
        timetable = Timetable.objects.get(title=timetable_title)
        old_lessons = Lesson.objects.filter(day__lessons__discipline__timetable=timetable.id)
        old_days = timetable.days.all()

        for day in old_days:
            timetable.days.remove(day)
            day.delete()

        for lesson in old_lessons:
            lesson.delete()
        return HttpResponse(asjson.dumps({}), content_type="application/json")

    @classmethod
    def update_setting(cls, metadata):
        timetable_title = metadata.get('timetable')
        segment_days = int(metadata.get('segmentDays'))
        complex_pair_in_day = int(metadata.get('complexPairInDay'))
        timetable = Timetable.objects.get(title=timetable_title)
        timetable.segment_days = segment_days
        timetable.complex_pairs_in_day = complex_pair_in_day
        timetable.save()

        return HttpResponse(json.dumps({"status": 'update'}), content_type="application/json")

    @classmethod
    def delete(cls, timetable_id):
        Timetable.objects.get(id=timetable_id).delete()
        return HttpResponse(json.dumps({"status": 'delete'}), content_type="application/json")
