from django.conf.urls import patterns
from django.conf.urls import url

from timetable.engine import get_schedule, export_to_excel
from timetable.models.timetable import Api
from .views import get_list_or_create_timetable, disciplines, tutors, auditories, specialities, courses, \
    disciplines_courses, groups, pairs, study_days, other_days, timetable, timetable_for_group

urlpatterns = patterns('',
                       url(r'^$', get_list_or_create_timetable, name='create_table'),
                       url(r'^timetables/$', get_list_or_create_timetable, name='create_table'),
                       url(r'^api/v1/timetables/(?P<timetable_title>[-\w\s\.]+)/$', get_schedule),
                       url(r'^api/v1/timetables/(?P<timetable_title>[-\w\s\.]+)/pages/(?P<page_number>\d+)/$', get_schedule),
                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/disciplines/$', disciplines, name='disciplines'),
                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/tutors/$', tutors, name='tutors'),
                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/auditories/$', auditories, name='auditories'),
                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/specialities/$', specialities, name='specialities'),
                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/courses/$', courses, name='courses'),
                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/disciplines_courses/$', disciplines_courses,
                           name='disciplines_courses'),
                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/groups/$', groups, name='groups'),
                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/pairs/$', pairs, name='pairs'),
                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/days/study/$', study_days, name='timetable'),
                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/days/other/$', other_days, name='timetable'),
                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/(?P<study_group>[-\w\s\.]+)/$', timetable_for_group, name='timetable_for_group'),
                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/$', timetable, name='timetable'),

                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/xls$', export_to_excel, name='export_to_excel'),
                       url(r'^timetables/(?P<timetable_title>[-\w\s\.]+)/pages/(?P<page_number>\d+)/$', timetable, name='timetable'),
                       url(r'^api/v1/timetables/$', Api.dispatcher_timetables, name='dispatcher_timetables'),
                       )
