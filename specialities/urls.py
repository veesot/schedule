from django.conf.urls import url

from .models import Api

urlpatterns = [url(r'^$', Api.dispatcher_speciality),
               url(r'^(?P<speciality_id>\d+)/$', Api.dispatcher_speciality), ]
