import json

from django.db import models
from django.http import HttpResponse

from courses.models import Course, LearningDiscipline
from disciplines.models import Discipline
from service.service import query_set_to_dict, instance_to_dict


class Speciality(models.Model):
    title = models.CharField(max_length=50, verbose_name='Специальность')
    courses = models.ManyToManyField(Course, verbose_name="Список курсов по специальности", blank=True)

    def __str__(self):
        return self.title

    class Meta:
        db_table = 'specialities'
        app_label = 'specialities'


class Api:
    @classmethod
    def dispatcher_speciality(cls, request, speciality_id=None):

        if request.method == 'GET' and speciality_id is None:  # Пришел запрос на индекс
            return cls.index(request)
        if request.method == 'GET' and speciality_id is not None:  # Пришел запрос на индекс
            return cls.get(speciality_id)
        elif request.method == 'POST':  # Запрос на создание
            return cls.add(request)
        elif request.method == 'DELETE' and speciality_id:
            return cls.remove(speciality_id)
        elif request.method == 'PATCH':
            return cls.update(request, speciality_id)

    @classmethod
    def index(cls, request):
        courses = request.GET.get('courses', None)
        disciplines = request.GET.get('disciplines', None)
        groups = request.GET.get('groups', None)
        timetable = request.GET.get('timetable', None)
        from timetable.models.timetable import Timetable
        current_timetable = Timetable.objects.get(title=timetable)
        specialities_all = current_timetable.specialities.all()
        specialities = query_set_to_dict(specialities_all, 'title', 'id')
        for speciality in specialities:
            current_speciality = Speciality.objects.get(id=speciality['id'])
            if courses == 'true':  # Нужны  курсы
                speciality['courses'] = query_set_to_dict(current_speciality.courses.all())
                if disciplines == 'true':  # Дополнительно  нужны дисциплины по курсам
                    for course in speciality['courses']:
                        course['disciplines'] = []
                        current_course = Course.objects.get(id=course['id'])
                        all_disciplines = current_course.learningdiscipline_set.all()
                        for discipline in all_disciplines:
                            # Также отслеживаем дисциплину от которых зависит текущая
                            depend_discipline = discipline.depend.title if discipline.depend_id else ''
                            course['disciplines'].append({'title': discipline.discipline.title,
                                                          'duration': discipline.duration,
                                                          'depend': depend_discipline,
                                                          'id': discipline.id})
                if groups == 'true':  # Нужна информация по группам
                    for course in speciality['courses']:
                        course['groups'] = []
                        current_course = Course.objects.get(id=course['id'])
                        all_groups = current_course.groups.all()
                        for group in all_groups:
                            course['groups'].append({'title': group.title, 'id': group.id})
        return HttpResponse(json.dumps(specialities), content_type="application/json")

    @classmethod
    def get(cls, speciality_id):
        response = {}
        speciality = Speciality.objects.get(id=speciality_id)
        response['title'] = speciality.title
        response['id'] = speciality.id
        response['courses'] = query_set_to_dict(speciality.courses.all())
        for course in response['courses']:
            course['disciplines'] = []
            current_course = Course.objects.get(id=course['id'])
            disciplines = query_set_to_dict(current_course.learningdiscipline_set.all())
            for discipline in disciplines:
                depend_discipline_id = discipline['depend_id'] if discipline['depend_id'] else ''
                if depend_discipline_id:
                    depend_discipline = Discipline.objects.get(id=depend_discipline_id).title
                else:
                    depend_discipline = ''
                course['disciplines'].append(
                    {'discipline': Discipline.objects.get(id=discipline['discipline_id']).title,
                     'duration': discipline['duration'],
                     'depend': depend_discipline,
                     'id': discipline['id']})

        return HttpResponse(json.dumps(response), content_type="application/json")

    @staticmethod
    def add(request):
        """
        Добавляем специальность
        """
        metadata = json.loads(request.body.decode())
        title = metadata['title']
        timetable = metadata['timetable']

        if not Speciality.objects.filter(title=title).exists():  # На случай если захотят добавят дубль
            speciality = Speciality()
            speciality.title = title
            speciality.save()

        speciality = Speciality.objects.get(title=title)
        from timetable.models.timetable import Timetable
        timetable = Timetable.objects.get(title=timetable)
        if speciality not in timetable.specialities.all():
            timetable.specialities.add(speciality)
            response = {'title': title, 'id': speciality.id}
        else:
            response = {'title': title, 'id': None}

        return HttpResponse(json.dumps(response), content_type="application/json")

    @staticmethod
    def update(request, speciality_id):
        """
        Обновляем метаинфо по специальности

        """

        metadata = json.loads(request.body.decode())
        if 'course' in metadata:
            course_meta_info = metadata['course']

            speciality = Speciality.objects.get(id=speciality_id)
            course = Course.objects.get(**course_meta_info)
            speciality_courses = speciality.courses.all()

            if course in speciality_courses:
                speciality.courses.remove(course)
                # Удаляем также из курсов
                course.delete()
            else:
                speciality.courses.add(course)

            return HttpResponse(json.dumps(query_set_to_dict(speciality.courses.all())),
                                content_type="application/json")
        elif 'discipline' in metadata:  # Привязка дисциплины
            discipline = metadata['discipline']
            discipline_course = LearningDiscipline()
            discipline_course.course = Course.objects.get(id=discipline['course'])
            discipline_course.discipline = Discipline.objects.get(title=discipline['discipline'])
            if discipline['depend']:  # Нужно указать дисциплину от которой зависим
                discipline_course.depend = Discipline.objects.get(title=discipline['depend'])

            discipline_course.duration = discipline['duration']
            discipline_course.save()
            response = instance_to_dict(discipline_course, 'id', 'discipline', 'depend', 'duration')
            response['discipline_id'] = response['id']
            response.pop("id", None)
            return HttpResponse(json.dumps(response), content_type="application/json")

        elif 'removedDisciplineId' in metadata:
            removed_discipline_id = metadata['removedDisciplineId']
            discipline = LearningDiscipline.objects.get(id=removed_discipline_id)
            discipline.delete()
            return HttpResponse(
                json.dumps(instance_to_dict(discipline, 'discipline_id', 'discipline', 'depend', 'duration')),
                content_type="application/json")

    @staticmethod
    def remove(speciality_id):
        """
        Удаляем специальность
        """
        speciality = Speciality.objects.get(id=speciality_id)
        timetable = speciality.timetable_set.get()
        # Если мы удаляем специальность - курсы завязаные на неё - нам тоже не нужны.
        courses_speciality = speciality.courses.all()
        for course in courses_speciality:
            course.delete()
        # Вычеркнем специальность из расписания.
        timetable.specialities.remove(speciality)
        # Сотрем все упоминаиния о ней
        speciality.delete()

        response = {'title': speciality.title, 'id': None}

        return HttpResponse(json.dumps(response), content_type="application/json")
