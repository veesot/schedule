#!/bin/bash

NAME="pet_project" # Name of the application
DJANGODIR=/home/user # Django project directory
SOCKFILE=run/gunicorn.sock # we will communicte using this unix socket
USER=user # the user to run as
GROUP=user # the group to run as
NUM_WORKERS=3 # how many worker processes should Gunicorn spawn
DJANGO_SETTINGS_MODULE=main.settings # which settings file should Django use
DJANGO_WSGI_MODULE=main.wsgi # WSGI module name

${DJANGODIR}/env/bin/python  ${DJANGODIR}/manage.py collectstatic --noinput

echo "Starting $NAME as `whoami`"

# Activate the virtual environment
cd ${DJANGODIR}
source ../env/bin/activate
export DJANGO_SETTINGS_MODULE=${DJANGO_SETTINGS_MODULE}
export PYTHONPATH=${DJANGODIR}:${PYTHONPATH}

# Create the run directory if it doesn't exist
RUNDIR=$(dirname ${SOCKFILE})
test -d ${RUNDIR} || mkdir -p ${RUNDIR}

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
exec  env/bin/gunicorn ${DJANGO_WSGI_MODULE}:application \
--name ${NAME} \
--workers ${NUM_WORKERS} \
--user=${USER} --group=${GROUP} \
--bind=0.0.0.0:8001 \
--log-level=debug \
--log-file=-
