from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
                       url(r'^', include('timetable.urls')),
                       url(r'^api/v1/', include('api.urls')),
)
