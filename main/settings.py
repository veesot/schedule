# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import socket
import locale
import os
from django.conf import global_settings

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

PROJECT_ROOT = os.path.realpath(os.path.dirname(__file__))

# SECURITY WARNING: keep the secret key used in production secret!
# Application definition

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    # 'django.contrib.messages',
    # 'django.contrib.admin',
    'django.contrib.staticfiles',

    'auditories',
    'courses',
    'specialities',
    'disciplines',
    'pairs',
    'study_groups',
    'days',
    'timetable',

)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.


# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/



TEMPLATE_CONTEXT_PROCESSORS = global_settings.TEMPLATE_CONTEXT_PROCESSORS + (
    "service.context.get_sub_domain",
)

import locale
import os

locale.setlocale(locale.LC_ALL,'ru_RU.UTF-8')
ACCOUNTS_TOKEN = 'Token 2cb39149ad82f39253b54a4f561ec6ff27185510'
MAIN_SERVER = os.getenv('MAIN_SERVER', 'http://localhost:8000/')
ACCOUNTS_SERVER = os.getenv('ACCOUNTS_SERVER', 'http://localhost:8001/')
SCHEDULE_SERVER = os.getenv('SCHEDULE_SERVER', 'http://localhost:8002/')
GROUPS_API = ACCOUNTS_SERVER + 'api/groups/'
ACCOUNTS_API = ACCOUNTS_SERVER + 'api/accounts/'
TUTORS_API = ACCOUNTS_SERVER + 'api/tutors/'
STUDENT_API = ACCOUNTS_SERVER + 'api/students/'
DB_HOST = os.getenv('DB_HOST', 'localhost')
SECRET_KEY = os.getenv('SECRET_KEY', 'super_secret_key')

ROOT_URLCONF = 'main.urls'
WSGI_APPLICATION = 'main.wsgi.application'
TEMPLATE_DIRS = ('templates',)
STATIC_URL = '/assets/'
STATIC_ROOT = BASE_DIR + STATIC_URL

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

DEBUG = True

TEMPLATE_DEBUG = True
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'spark',
        'USER': 'root',
        'PASSWORD': "toor",
        'HOST': DB_HOST,  # Set to empty string for localhost.
        'PORT': '',  # Set to empty string for default.
        'TEST': {
            'NAME': 'test',
        },
    },

}
TIME_ZONE = 'Asia/Yekaterinburg'
USE_TZ = True

LANGUAGE_CODE = 'ru-RU'

SHORT_DATE_FORMAT = 'd E'
TIME_FORMAT = 'H:i'

LANGUAGES = (
    ('ru', 'Russian'),
    ('en', 'English'),
)
USE_I18N = True
USE_L10N = False
