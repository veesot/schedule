import json
from django.db import models
from django.http import HttpResponse
from service.service import query_set_to_dict, instance_to_dict


class StudyGroup(models.Model):
    title = models.CharField(verbose_name="Название студенческой группы", max_length=10)

    def __str__(self):
        return self.title

    class Meta:
        app_label = 'study_groups'
        db_table = 'study_groups'
        verbose_name = 'Студенческая группа'
        verbose_name_plural = "Студенческие группы"


class Api:
    @classmethod
    def dispatcher_group(cls, request, group_id=None):

        if request.method == 'GET' and group_id is None:  # Пришел запрос на индекс
            return cls.index()
        if request.method == 'GET' and group_id is not None:  # Пришел запрос на конкретную группу
            return cls.get(group_id)
        elif request.method == 'POST':  # Запрос на создание
            metadata = json.loads(request.body.decode())
            return cls.add(metadata)
        elif request.method == 'DELETE' and group_id:
            return cls.remove(group_id)

    @classmethod
    def index(cls):
        groups = query_set_to_dict(StudyGroup.objects.all())
        return HttpResponse(json.dumps(groups), content_type="application/json")

    @classmethod
    def get(cls, speciality_id):
        group = StudyGroup.objects.get(id=speciality_id)
        response = instance_to_dict(group, 'title', 'id')
        return HttpResponse(json.dumps(response), content_type="application/json")

    @staticmethod
    def add(metadata):
        """Создание группы и привязка её к определеному курсу"""
        from courses import Course
        title = metadata['title']
        if not StudyGroup.objects.filter(title=title).exists():  # Если не было ранее такой группы
            group = StudyGroup()
            group.title = title
            group.save()

            group.course_set.add(Course.objects.get(id=metadata['courseId']))  # Опосредовано добавим к курсу
            response = instance_to_dict(group, 'title', 'id')
        else:
            group = StudyGroup.objects.get(title=title)
            course = Course.objects.get(id=metadata.get('courseId'))
            if not (group in course.groups.all()):
                course.groups.add(group)
            response = instance_to_dict(group, 'title', 'id')  # Если группа уже есть - нам нет смысла создавать её заново.Отдадим
        return HttpResponse(json.dumps(response), content_type="application/json")

    @staticmethod
    def remove(group_id):
        group = StudyGroup.objects.filter(id=group_id)
        group.delete()
        response = []
        return HttpResponse(json.dumps(response), content_type="application/json")
