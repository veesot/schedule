from django.conf.urls import url

from .models import Api

urlpatterns = [url(r'^$', Api.dispatcher_group),
               url(r'^(?P<group_id>\d+)/$', Api.dispatcher_group)]
