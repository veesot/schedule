import datetime
import json
from typing import Dict

from django.db import models
from django.http import HttpResponse

from service.service import query_set_to_dict


class Pair(models.Model):
    title = models.CharField(max_length=20, verbose_name='Наименование пары')
    beginning_of_period = models.TimeField(verbose_name='Дата начала действия')
    end_of_period = models.TimeField(verbose_name='Дата окончания действия')

    def __str__(self):
        return self.title

    class Meta:
        app_label = 'pairs'
        db_table = 'pairs'


class Api:
    @classmethod
    def dispatcher_pairs(cls, request, pair_id=None):
        if request.method == 'GET':
            return cls.get_pairs()
        elif request.method == 'POST':
            metadata = json.loads(request.body.decode())
            return cls.add(metadata)
        elif request.method == 'DELETE' and pair_id:
            return cls.remove(pair_id)

    @classmethod
    def get_pairs(cls):
        all_pairs = query_set_to_dict(Pair.objects.all())
        for pair in all_pairs:
            pair['beginning_of_period'] = pair['beginning_of_period'].strftime("%H:%M")
            pair['end_of_period'] = pair['end_of_period'].strftime("%H:%M")
        return HttpResponse(json.dumps(all_pairs), content_type="application/json")

    @classmethod
    def add(cls, metadata: Dict[str, str]):
        timetable_title = metadata.get('timetable')
        from timetable.models import Timetable
        timetable = Timetable.objects.get(title=timetable_title)

        pair = Pair()
        pair.title = metadata['title']
        pair.beginning_of_period = datetime.datetime.strptime(metadata['beginning_of_period'], "%H:%M")
        pair.end_of_period = datetime.datetime.strptime(metadata['end_of_period'], "%H:%M")
        pair.save()

        timetable.pairs.add(pair)

        response = {'id': pair.id, 'title': pair.title,
                    'beginning_of_period': pair.beginning_of_period.strftime("%H:%M"),
                    'end_of_period': pair.end_of_period.strftime("%H:%M")}

        return HttpResponse(json.dumps(response), content_type="application/json")

    @classmethod
    def remove(cls, pair_id):
        pair = Pair.objects.get(id=pair_id)
        timetable = pair.timetable_set.get()
        timetable.pairs.remove(pair)
        pair.delete()
        response = []
        return HttpResponse(json.dumps(response), content_type="application/json")
