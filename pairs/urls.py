from django.conf.urls import url

from .models import Api

urlpatterns = [url(r'^$', Api.dispatcher_pairs),
               url(r'^(?P<pair_id>\d+)/$', Api.dispatcher_pairs)]
